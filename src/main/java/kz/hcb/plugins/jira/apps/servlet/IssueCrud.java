package kz.hcb.plugins.jira.apps.servlet;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import com.atlassian.templaterenderer.RenderingException;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Scanned
public class IssueCrud extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(IssueCrud.class);

    private final static String ISSUE_TEMPLATE_SRC = "/templates/issue.vm";

    @JiraImport
    private TemplateRenderer templateRenderer;
    @JiraImport
    private PageBuilderService pageBuilderService;

    @Inject
    public IssueCrud(TemplateRenderer templateRenderer, PageBuilderService pageBuilderService) {
        this.templateRenderer = templateRenderer;
        this.pageBuilderService = pageBuilderService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            pageBuilderService.assembler().resources().requireWebResource("com.atlassian.auiplugin:ajs");

            Map<String, Object> context = Maps.newHashMap();
//            context.put("optionsContractType", getListOfStringOptions(CF_CONTRACT_TYPE_ID));
            resp.setContentType("text/html;charset=utf-8");
            templateRenderer.render(ISSUE_TEMPLATE_SRC, context, resp.getWriter());
        } catch (
                IOException e) {
            log.info("ERROR IOException - in doGet method, exception is " + e);
        } catch (
                RenderingException e) {
            log.info("ERROR RenderingException - in doGet method, exception is " + e);
        } catch (
                Exception e) {
            log.info("ERROR Exception - in doGet method, exception is " + e);
        }

    }
}