package kz.hcb.plugins.jira.apps;

import com.atlassian.extras.common.log.Logger;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.templaterenderer.RenderingException;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.Maps;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.Map;

@Scanned
public class CreateIssueServlet extends HttpServlet {
    private static final Logger.Log log = Logger.getInstance(CreateIssueServlet.class);

    @JiraImport
    private PageBuilderService pageBuilderService;
    @JiraImport
    private final TemplateRenderer renderer;
    @JiraImport
    private final LoginUriProvider loginUriProvider;

    @JiraImport
    private final UserManager userManager;

    // Constants
    private static final String CREATE_ISSUE_TEMPLATE = "templates/create-issue.vm";

    @Inject
    public CreateIssueServlet(PageBuilderService pageBuilderService, TemplateRenderer renderer, LoginUriProvider loginUriProvider, UserManager userManager) {
        this.pageBuilderService = pageBuilderService;
        this.renderer = renderer;
        this.loginUriProvider = loginUriProvider;
        this.userManager = userManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            pageBuilderService.assembler().resources().requireWebResource("com.atlassian.auiplugin:ajs");

            try {
                String username = userManager.getRemoteUser().getUsername();
                System.out.println("username is " + username);
                if (username == null) {
                    redirectToLogin(req, resp);
                    return;
                }
            } catch (IOException e) {
                redirectToLogin(req, resp);
            }

            Map<String, Object> context = Maps.newHashMap();
//            context.put("optionsContractType", getListOfStringOptions(CF_CONTRACT_TYPE_ID));
//            context.put("optionsProviderType", getListOfStringOptions(CF_PROVIDER_TYPE_ID));
//            context.put("optionsContractCurrency", getListOfStringOptions(CF_CONTRACT_CURRENCY_ID));
//            context.put("optionsContractForm", getListOfStringOptions(CF_CONTRACT_FORM_ID));
//            context.put("optionsProcurementType", getListOfStringOptions(CF_PROCUREMENT_TYPE_ID));
//            context.put("optionsIsInfSecNeeded", getListOfStringOptions(CF_IS_INF_SEC_NEEDED_ID));
//            context.put("optionsInfSecAddCheck", listOfAdditionalChecksValues);
            resp.setContentType("text/html;charset=utf-8");
            renderer.render(CREATE_ISSUE_TEMPLATE, context, resp.getWriter());
//        renderer.render("templates/servlets/test.vm", context, resp.getWriter());
        } catch (IOException e) {
            log.info("ERROR IOException - in doGet method, exception is " + e);
            redirectToLogin(req, resp);
        } catch (RenderingException e) {
            log.info("ERROR RenderingException - in doGet method, exception is " + e);
            redirectToLogin(req, resp);
        } catch (Exception e) {
            log.info("ERROR Exception - in doGet method, exception is " + e);
            redirectToLogin(req, resp);
        }
    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    private URI getUri(HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }
}
