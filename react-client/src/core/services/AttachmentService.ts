import axios, { CancelToken } from "axios";

import { getAjsContextPath } from "../utils/jira.utils";

// const ATTACHMENT_API = `${getAjsContextPath()}/rest/hcb-ca/1/attachment`;
const ATTACHMENT_API = `${getAjsContextPath()}/rest/api/2/issue`;

interface UploadRequest {
  fid: string;
  files: File[];
}

const doUpload = (
  req: UploadRequest,
  progressCallback: (percentCompleted: number) => void,
  cancelToken: CancelToken
): Promise<UploadRequest> => {
  const formData: FormData = new FormData();
  formData.append("fid", req.fid);
  req.files.forEach((file) => {
    formData.append("file", file);
  });

  const config = {
    headers: {
      "X-Atlassian-Token": "no-check",
      "Content-Type": "multipart/form-data; charset=utf-8",
    },
    onUploadProgress: (progressEvent) => {
      const percentCompleted = Math.round(
        (progressEvent.loaded * 100) / progressEvent.total
      );
      progressCallback(percentCompleted);
    },
    cancelToken,
  };

  return axios
    // .post(`${ATTACHMENT_API}?fid=${req.fid}`, formData, config)
    .post(`${ATTACHMENT_API}/${req.fid}/attachments`, formData, config)
    .then((response) => response.data);
};

export const AttachmentService = {
  doUpload,
};
