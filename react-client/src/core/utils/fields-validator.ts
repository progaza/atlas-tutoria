export function validateTextField(value: String) {
    // console.log('value is', value);

    if (!value || value === 'null' || value === '') return 'Обязательно к заполнению!';

    if (value.length < 3) return 'Длинееееееее!!!!';

    return  undefined;
}

export function validateNumberTextField(value: String) {
    if(!value) return 'Обязательно к заполнению!';

    if(Number(value) < 0) return 'Значение должно быть положительным';

    return undefined;
}
