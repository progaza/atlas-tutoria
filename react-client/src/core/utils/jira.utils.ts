import MyOptionInterface from "../../models/MyOptionInterface";

// declare let AJS: any;
declare const window: any;

if (typeof window.AJS == "undefined") {
  window.AJS = {
    Meta: {
      get(key: string) {
        return key === "user-locale" ? "en-US" : key;
      }
    }
  };
}

export function getAjsContextPath() {
  return getAjsMeta("context-path");
}

export function getAjsMeta(key: string) {
  return window.AJS.Meta.get(key);
}

export function parse_query_string(query) {
  var vars = query.split("&");
  var query_string = {};
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    var key = decodeURIComponent(pair[0]);
    var value = decodeURIComponent(pair[1]);
    // If first entry with this name
    if (typeof query_string[key] === "undefined") {
      query_string[key] = decodeURIComponent(value);
      // If second entry with this name
    } else if (typeof query_string[key] === "string") {
      var arr = [query_string[key], decodeURIComponent(value)];
      query_string[key] = arr;
      // If third or later entry with this name
    } else {
      query_string[key].push(decodeURIComponent(value));
    }
  }
  return query_string;
}

export const getIssueKey = () => {
  let result = ''
  try {
    const query: string = window.location.search.substring(1);
    const params: { issueKey?: '' } = parse_query_string(query);
    result = (params.issueKey) ? params.issueKey : ''
  } catch (err) {
    console.error('getIssueKey error', err)
  } finally {
    return result
  }
}

export const getIssueAction = (): string | undefined => {
  let result;
  try {
    const query: string = window.location.search.substring(1);
    const params: { issueAction?: '' } = parse_query_string(query);
    result = (params.issueAction) ? params.issueAction : ''

    return result
  } catch (err) {
    console.error('getIssueAction error', err)
  } finally {
    return result
  }

}

export const getCurrentDate = () => {
  const today = new Date();
  const dd = String(today.getDate()).padStart(2, '0');
  const mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  const yyyy = today.getFullYear();
  return mm + '/' + dd + '/' + yyyy;
}

export const compareOptionsByLabel = (a: MyOptionInterface, b: MyOptionInterface) => {
  if (a.label < b.label) {
    return -1;
  }
  if (a.label > b.label) {
    return 1;
  }
  return 0;
}

export const getSumFromTableData = (tableData2Value) => {
  let initialSumValue
  if (tableData2Value?.length === 1) {
    initialSumValue = (tableData2Value[0].row) ? tableData2Value[0].row.sum : tableData2Value[0].sum;
  } else if (tableData2Value?.length > 1) {
    initialSumValue = tableData2Value.reduce((accumulator, item) => {
      if (accumulator?.row) {
        return Number(accumulator?.row?.sum) + Number(item?.row?.sum)
      } else if (accumulator?.sum) {
        return Number(accumulator?.sum) + Number(item?.sum)
      } else {
        return Number(accumulator) + Number((item?.row?.sum) ? item?.row?.sum : item?.sum)
      }
    });
  } else {
    initialSumValue = 0
  }

  return initialSumValue
}