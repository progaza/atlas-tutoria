export const ENVIRONMENT_PRODUCTION = 'production';

export let environment;
environment = 'production'; // PROD
// environment = 'test'; // TEST

let jiraBaseUrl;
if (environment === ENVIRONMENT_PRODUCTION) {
    jiraBaseUrl = 'https://jira.homecredit.kz'
} else {
    jiraBaseUrl = 'http://10.8.53.35:8080'
}

export enum IssueActionTypes {
    COPY, UPDATE
}

const ProjectConstants = {
    'urls': {
        'createIssue': jiraBaseUrl + '/rest/api/2/issue',
        'getDataFrom1C': jiraBaseUrl + '/rest/scriptrunner/latest/custom/getDataForPaymentOrder',
        'getUserData': jiraBaseUrl + '/rest/scriptrunner/latest/custom/getUserData',
        'getApplicationForSpending': jiraBaseUrl + '/rest/scriptrunner/latest/custom/getApplicationForSpending',
        'getProviderAndContractData': jiraBaseUrl + '/rest/scriptrunner/latest/custom/getLinkedContractNumbers', // ?q=050540000690
        'postCreatePaymentOrderTicket': jiraBaseUrl + '/rest/scriptrunner/latest/custom/postCreatePaymentOrderTicket', // ?q=050540000690
        'issueBaseUrl': jiraBaseUrl + '/browse',
        'searchProvider': jiraBaseUrl + '/rest/scriptrunner/latest/custom/getSearchProvider',
        'updateIssue': jiraBaseUrl + '/rest/scriptrunner/latest/custom/putUpdateIssue'
    },
    'users': {
        'jira': {
            'login': 'jira-system',
            'pw': 'Z+123qwe'
        }
    },
    'projectKey': 'PO',
    'issuetypeId': '11501',
    'fields': {
        'totalSum': (environment === ENVIRONMENT_PRODUCTION) ? 'customfield_' + 36500 : 'customfield_' + 32100,
        'issueInBudgetFlag': (environment === ENVIRONMENT_PRODUCTION) ? 'customfield_' + 27904 : 'customfield_' + 23232,
        'provider': (environment === ENVIRONMENT_PRODUCTION) ? 'customfield_' + 20934 : 'customfield_' + 23306,
        'contractNumber': (environment === ENVIRONMENT_PRODUCTION) ? 'customfield_' + 10207 : 'customfield_' + 10207,
        'budgetYear': (environment === ENVIRONMENT_PRODUCTION) ? 'customfield_' + 27901 : 'customfield_' + 26901,
        'contactData': (environment === ENVIRONMENT_PRODUCTION) ? 'customfield_' + 31509 : 'customfield_' + 27219,
        'providerId': (environment === ENVIRONMENT_PRODUCTION) ? 'customfield_' + 26500 : 'customfield_' + 23305,
        'region': (environment === ENVIRONMENT_PRODUCTION) ? 'customfield_' + 36200 : 'customfield_' + 31402,
        'currency': (environment === ENVIRONMENT_PRODUCTION) ? 'customfield_' + 33100 : 'customfield_' + 31401,
        'contractSum': (environment === ENVIRONMENT_PRODUCTION) ? 'customfield_' + 20937 : 'customfield_' + 21902,
        'procurementType': (environment === ENVIRONMENT_PRODUCTION) ? 'customfield_' + 26812 : 'customfield_' + 25007,
        'systemTextStorage': (environment === ENVIRONMENT_PRODUCTION) ? 'customfield_' + 36206 : 'customfield_' + 31704,
        'tableData_1': (environment === ENVIRONMENT_PRODUCTION) ? 'customfield_' + 27908 : 'customfield_' + 26710,
        'tableData_2': (environment === ENVIRONMENT_PRODUCTION) ? 'customfield_' + 36201 : 'customfield_' + 31600,
        'guid': (environment === ENVIRONMENT_PRODUCTION) ? 'customfield_' + 27907 : 'customfield_' + 23800,
        'systemTextStorage_0': (environment === ENVIRONMENT_PRODUCTION) ? 'customfield_' + 25401 : 'customfield_' + 25017,
    },
    'issueActionTypes': {
        'update': 'update',
        'copy': 'copy',
    }
}

export enum RequiredFieldTypes {
    SIMPLE,
    OPTION,
    TABLE_ROW,
    GENERAL,
};

export class RequiredFieldModel {
    type: RequiredFieldTypes
    title: string
    errorMessage: string
    rowNumber: string

    constructor(title: string, type: RequiredFieldTypes, rowNumber: string = '', errorMessage: string = '') {
        this.type = type;
        this.title = title;
        this.rowNumber = rowNumber;
        if (type === RequiredFieldTypes.TABLE_ROW) this.errorMessage = `Поле "${title}", в разделе ${Number(rowNumber) + 1} не заполнено!`;
        else if (type === RequiredFieldTypes.GENERAL) this.errorMessage = errorMessage;
        else this.errorMessage = `Поле "${title}" обязательно к заполнению!`;
    }
}

export const RequiredFieldsData = {
    db1: {
        'reqFieldNames': ['provider', 'contractNum', 'applicationForSpending', 'priority'],
        'provider': new RequiredFieldModel('Поставщик', RequiredFieldTypes.OPTION),
        'contractNum': new RequiredFieldModel('Номер контракта', RequiredFieldTypes.OPTION),
        'applicationForSpending': new RequiredFieldModel('Заявка на расходования средств', RequiredFieldTypes.OPTION),
        'priority': new RequiredFieldModel('Приоритет', RequiredFieldTypes.OPTION),
    },
    bd3: {
    },
    db4: {
        // 'region': new RequiredFieldModel('Регион', RequiredFieldTypes.OPTION),
        // 'budgetYear': new RequiredFieldModel('Бюджетный год', RequiredFieldTypes.OPTION),
        'description': new RequiredFieldModel('Описание', RequiredFieldTypes.SIMPLE),
        'comment': new RequiredFieldModel('Комментарий к доработке', RequiredFieldTypes.SIMPLE),
    },
    edit: {
        'sumIncreasedError': new RequiredFieldModel('sumIncreasedError', RequiredFieldTypes.GENERAL, '', `Увеличение суммы заявки недопустимо.
        Перепроверьте, пожалуйста, блок 3, внесите изменения.
        После чего, нажмите кнопку "Отправить"`),
    },
    sumHasIllegalChars: {
        'sumHasIllegalChars': new RequiredFieldModel('sumHasIllegalChars', RequiredFieldTypes.GENERAL, '', `Поле сумма в блоке 3 заполненно не корректно!`),
    }
};

export default ProjectConstants;