export const getApiCall = (link) => {
    return fetchRetry(link, 1000, 3, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json;charset=UTF-8'
        }
    }).then(response => response.json());
}

function wait(delay: number) {
    return new Promise((resolve) => setTimeout(resolve, delay));
}

function fetchRetry(url: string, delay: number, tries: number, fetchOptions = {}) {
    console.log('fetchRetry', 'tries', tries);

    function onError(err) {
        const triesLeft = tries - 1;
        if (!triesLeft) {
            throw err;
        }
        return wait(delay).then(() => fetchRetry(url, delay, triesLeft, fetchOptions));
    }
    return fetch(url, fetchOptions).catch(onError);
}

export const postApiCall = (link, requestBody) => {
    return fetch(link, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json;charset=UTF-8',
        },
        body: requestBody
    }).then(response => response.json());
}

export const putApiCall = (link, requestBody) => {
    return fetch(link, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json;charset=UTF-8'
        },
        body: requestBody
    }).then(response => {
        return response
    }
    );
}

