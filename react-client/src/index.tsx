import React from "react";
import ReactDOM from "react-dom";
import {Issue} from "./containers/issue";

document.addEventListener("DOMContentLoaded", () => {
  const vSettingNode = document.getElementById("test-react-id");
  if (vSettingNode) {
    ReactDOM.render(<Issue />, vSettingNode);
  }
});