import "./FileList.css";

import React from "react";

import { FileSize } from "./FileSize";

interface Props {
  files: File[];
  inline?: boolean;
  onDelete?(file: File): void;
}

function FileList({ files, inline }: Props) {
  return (
    <div className={`file-list ${inline ? "file-list--inline" : ""}`}>
      {files.map((file) => (
        <div key={file.name} className="file-list__item">
          {file.name}{" "}
          <span style={{ color: "#666", fontSize: ".85em" }}>
            <FileSize bytes={file.size} />
          </span>
        </div>
      ))}
    </div>
  );
}

export default FileList
