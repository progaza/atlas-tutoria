import React, { Fragment } from 'react'
import { OptionType, ValueType } from '@atlaskit/select';
import { Field, ErrorMessage } from '@atlaskit/form';
import FileList from './FileList';

interface IProps {
    files?: File[];
    attachmentUploadProgress: number;
    onSelectFiles(files: File[]): void;
}

function Attachment({
    files,
    attachmentUploadProgress,
    onSelectFiles, }: IProps) {

    return (
        <Field<ValueType<OptionType>>
            name='attachment'
            label='Вложение'
            isRequired
        >
            {({ fieldProps: { id }, error }) => (
                <Fragment>
                    <br />
                    <fieldset style={{ marginTop: "8px", padding: "8px" }}>

                        <label className="aui-button btn-select-attachment">
                            <input
                                hidden
                                multiple
                                type="file"
                                name="attachments"
                                onChange={(e) => onSelectFiles(Array.from(e.target.files || []))}
                            />
                            <span>Выберите файлы</span>
                        </label>
                        {files && <FileList files={files} />}
                        {attachmentUploadProgress > 0 && (
                            <div style={{ color: "red" }}>
                                Загрузка {attachmentUploadProgress}%
                            </div>
                        )}
                    </fieldset>
                    {error && <ErrorMessage>{error}</ErrorMessage>}
                </Fragment>
            )}
        </Field>

    );
}

export default Attachment;