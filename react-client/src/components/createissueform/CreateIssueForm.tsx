import React, { Component } from 'react';
import DataBlock1 from '../datablocks/db1/DataBlock1';
import DataBlock2 from '../datablocks/db2/DataBlock2';
import DataBlock3 from '../datablocks/db3/DataBlock3';
import DataBlock4 from '../datablocks/db4/DataBlock4';

import { getAjsMeta, getIssueAction, getIssueKey, getSumFromTableData } from "../../core/utils/jira.utils";
import POCustomFields from '../../models/POCustomFields';
import CustomField from '../../models/CustomField';
import { getApiCall } from '../../core/api/api';
import ProjectConstants from '../../core/constants/ProjectConstants';
import MyOptionInterface from '../../models/MyOptionInterface';
import { Db2MyTableStateInterface, Db2TabledataInterface } from '../../models/Db2stateInterface';
import { MyStoreInterface } from '../../models/MyStoreInterface';
import { Db3MyTableStateInterface } from '../../models/Db3stateInterface';


let customFields: POCustomFields;

const { urls, fields, issueActionTypes } = ProjectConstants

interface IProps {
  passState?: any,
  selectedBlock?: number
  files?: File[];
  attachmentUploadProgress: number;
  onSelectFiles(files: File[]): void;
}

class CreateIssueForm extends Component<IProps, MyStoreInterface> {
  constructor(props) {
    super(props);

    this.state = {
      db1state: {
        datablock: 1,
        chosenContract: {
          label: '',
          currency: '',
          sum: '',
          procurementType: '',
          value: '',
        },
      },
      db2state: {
        datablock: 2,
        tableData: [{ idRow: 1, documentType: '', documentNum: '', documentDate: '' }],
      },
      db3state: {
        tableData: [],
        options: {
          clients: {
            available: [],
            isLoading: false,
          },
          organizationUnits: {
            available: [],
            isLoading: false,
          },
          nomenclatures: {
            available: [],
            isLoading: false,
            nomenAndUnitsLink: [{
              nomenId: '',
              unitId: '',
            }],
          },
          nomenclatureCharacteristics: {
            available: new Map(),
            isLoading: false,
          },
          unitOfMeasurements: {
            available: [],
            isLoading: false,
          },
          budgetItems: {
            available: [],
            isLoading: false,
          },
        },
      },
      db4state: {
        budgetYear: { label: '', value: '' },
        providerContacts: '',
        description: '',
        region: {
          available: [],
          isLoading: true,
          value: { label: '', value: '' }
        },
      },
      tableData: '',
      currencyData: '',
      wasAlreadyPrefilled: false,
      isLoading: true,
    }

    this.passState = this.passState.bind(this);
    this.passApplicationForSpendingTableData = this.passApplicationForSpendingTableData.bind(this);
    this.passCurrencyData = this.passCurrencyData.bind(this);
    this.preFillForm = this.preFillForm.bind(this);
  }

  render() {
    const tableData = this.state.tableData;
    const currencyData = this.state.currencyData;
    const { files, attachmentUploadProgress } = this.props;

    return (
      <div>
        {this.props.selectedBlock === 1 &&
          <DataBlock1 curState={this.state.db1state} passState={this.passState} passApplicationForSpendingTableData={this.passApplicationForSpendingTableData} passCurrencyData={this.passCurrencyData} />
        }
        {this.props.selectedBlock === 2 &&
          <DataBlock2 curState={this.state.db2state} passState={this.passState} />
        }
        {this.props.selectedBlock === 3 &&
          <DataBlock3 curState={this.state.db3state} dataFromOtherBlocks={{ tableData: tableData, currencyData: currencyData }} passState={this.passState} />
        }
        {this.props.selectedBlock === 4 &&
          <DataBlock4 curState={this.state.db4state} passState={this.passState} files={files} attachmentUploadProgress={attachmentUploadProgress} onSelectFiles={this.props.onSelectFiles} />
        }

      </div>
    );
  }

  componentDidMount() {
    // get issue key from path
    const issueKey: string = getIssueKey();

    let isLoadingVal = true;
    const wasAlreadyPrefilled = this.state.wasAlreadyPrefilled;
    if (issueKey !== '' && !wasAlreadyPrefilled) {
      this.preFillForm(issueKey);
    } else {
      isLoadingVal = false;
    }

    this.setState({
      isLoading: isLoadingVal
    });
  }

  componentDidUpdate() {
    this.props.passState(this.state);
  }

  passApplicationForSpendingTableData(tableData) {
    this.setState({
      tableData: tableData
    })
  }

  passCurrencyData(currencyData) {
    this.setState({
      currencyData: currencyData
    })
  }

  passState(data) {
    console.log('CreateIssueForm data', data)
    if (data.datablock === 1) {
      this.setState({
        db1state: data
      });

    } else if (data.datablock === 2) {
      this.setState({
        db2state: data
      });
    } else if (data.datablock === 3) {
      this.setState({
        db3state: data
      });
    } else if (data.datablock === 4) {
      this.setState({
        db4state: data
      });
    } else {
      console.error('Error passing data, datablock number is wrong');
    }
  }

  preFillForm(issueKey) {
    // get reporter of that issue, check if this user is not reporter
    let issueReporter: string = '';

    // get current user 
    const currentUser = getAjsMeta("remote-user");

    //check if form was already pre-filled
    getApiCall(urls.createIssue + '/' + issueKey)
      .then(data => {
        issueReporter = data.fields.reporter.name
        if (currentUser === issueReporter) {
          // else get issue data from storage
          const issueAction = getIssueAction();
          const isActionCopy = (issueAction === issueActionTypes.copy);

          const systemTextStorageJson = JSON.parse(data.fields[fields.systemTextStorage])

          const { application4Spending, contractsOptions, appl4spendingAvailableOptions,
            regionAvailableOptions
          } = systemTextStorageJson

          const provider = new CustomField(fields.provider, data.fields[fields.provider]);
          const contractNumber = new CustomField(fields.contractNumber, systemTextStorageJson[fields.contractNumber]);
          const contractNumberLabel = new CustomField(fields.contractNumber, data.fields[fields.contractNumber]);
          const region = new CustomField(fields.region, systemTextStorageJson[fields.region]);
          const budgetYear = new CustomField(fields.budgetYear, data.fields[fields.budgetYear]?.value);
          const providerContactData = new CustomField(fields.contactData, data.fields[fields.contactData]);
          const providerId = new CustomField(fields.providerId, data.fields[fields.providerId]);
          const currency = new CustomField(fields.currency, data.fields[fields.currency]);
          const contractSum = new CustomField(fields.contractSum, data.fields[fields.contractSum]);
          const procurementType = new CustomField(fields.procurementType, data.fields[fields.procurementType]?.value);
          const tableData = new CustomField(fields.tableData_1, (systemTextStorageJson[fields.tableData_1]) ? JSON.parse(systemTextStorageJson[fields.tableData_1])?.data : undefined);

          const tableData2Value = JSON.parse(systemTextStorageJson[fields.tableData_2])?.data
          const tableData2 = new CustomField(fields.tableData_2, tableData2Value);
          const tableData2Labels = new CustomField('tableData2Labels', JSON.parse(data.fields[fields.tableData_2]).data);
          const priority = new CustomField('priority', {
            label: data.fields.priority.name,
            value: data.fields.priority.id,
          });
          const application4SpendingCF = new CustomField('application4Spending', (isActionCopy) ? undefined : JSON.parse(application4Spending));
          const appl4spendingAvailableOptionsCF = new CustomField('appl4spendingAvailableOptions', JSON.parse(appl4spendingAvailableOptions));
          const contractsOptionsCF = new CustomField('contractsOptions', JSON.parse(contractsOptions));
          const regionAvailableOptionsCF = new CustomField('regionAvailableOptions', JSON.parse(regionAvailableOptions));
          const descriptionCF = new CustomField('description', data.fields.description);
          const guid = new CustomField(fields.guid, (isActionCopy) ? undefined : data.fields[fields.guid]);

          customFields = new POCustomFields(provider, contractNumber, contractNumberLabel, region, budgetYear, providerContactData, providerId, currency, contractSum, procurementType, tableData, tableData2, priority, application4SpendingCF, contractsOptionsCF, appl4spendingAvailableOptionsCF, regionAvailableOptionsCF, descriptionCF, tableData2Labels, guid);

          // and prefill form, but you need to add some data about labels of options
          this.prefillBlock1(customFields);



          if (!isActionCopy) this.prefillBlock2(customFields);

          this.prefillBlock3(customFields);

          this.prefillBlock4(customFields);

          // getting initial values - which were at the start of editing process
          let initialSumValue = getSumFromTableData(tableData2Value)

          // while it is happening should show spinner instead of page
          if (contractSum.value) {
            this.setState({
              isLoading: false,
              wasAlreadyPrefilled: true,
              validationData: {
                initialSum: initialSumValue
              }
            });
          } else {
            this.setState({
              isLoading: false,
              wasAlreadyPrefilled: true
            });
          }
        }
      });
  }

  prefillBlock1 = (customFields: POCustomFields) => {
    const providerOption: MyOptionInterface = {
      label: customFields.provider?.value as string,
      value: customFields.providerId?.value as string
    }

    this.setState({
      db1state: {
        datablock: 1,
        provider: {
          availableOptions: [
            providerOption
          ],
          // linkedContracts: customFields.contractsOptions?.value as MyOptionInterface[],
          linkedContracts: [],
          isValid: true,
          name: '',
          isLoading: false,
          chosen: {
            chosenProvider: providerOption
          },
        },
        chosenContract: {
          label: customFields.contractNumberLabel?.value as string,
          value: customFields.contractNumber?.value as string,
          currency: customFields.currency?.value as string,
          sum: customFields.contractSum?.value as string,
          procurementType: customFields.procurementType?.value as string
        },
        contractsOptions: customFields.contractsOptions?.value as MyOptionInterface[],
        application4Spending: {
          availableOptions: customFields.appl4spendingAvailableOptionsCF?.value as MyOptionInterface[],
          name: (customFields.application4Spending?.value as MyOptionInterface)?.label,
          value: ((customFields.application4Spending?.value as MyOptionInterface)?.value) ? (customFields.application4Spending?.value as MyOptionInterface).value : customFields.guid?.value as string,
          isLoading: false,
          providerForApplications: '',
          tableData: [],
        },
        priority: customFields.priority?.value as MyOptionInterface,
        isLoading: false
      }
    });
  }

  prefillBlock2 = (customFields: POCustomFields) => {
    const tableData = customFields.tableData?.value as Db2TabledataInterface[]
    if (tableData === undefined) return;

    // convert tableData to Db2MyTableStateInterface[]
    const convertedTableData: Db2MyTableStateInterface[] = tableData.map((item, index) => {
      const row: Db2MyTableStateInterface = {
        idRow: index,
        documentType: item.document.type,
        documentDate: item.document.date,
        documentNum: item.document.number,
      }
      return row
    });


    this.setState({
      db2state: {
        datablock: 2,
        tableData: convertedTableData,
      }
    });
  }

  prefillBlock3 = (customFields: POCustomFields) => {
    // get guid data from storage
    const tableData2 = customFields.tableData2?.value as [{ row: Db3MyTableStateInterface }];

    // get labels from tableData2
    const tableData2Labels = customFields.tableData2Labels?.value as [{ row: Db3MyTableStateInterface }];

    if (tableData2.length !== tableData2Labels.length) {
      console.error('Some kind of error, length of same tables is different');
      return;
    }

    let resultingTableData: Db3MyTableStateInterface[] = [{
      idRow: 0,
      client: '',
      organizationUnit: '',
      nomenclature: '',
      nomenclatureCharacteristic: '',
      unitOfMeasurement: '',
      productType: '',
      reportedYear: '',
      reportedMonth: '',
      sum: '',
      sumInKzt: '',
      amount: '',
      vatRate: '',
      budgetItem: '',
      application4SpendingGuid: '',
    }];

    const issueAction = getIssueAction();
    const isActionCopy = (issueAction === issueActionTypes.copy);

    // merge data into resulting table data
    for (let i = 0; i < tableData2.length; i++) {
      resultingTableData[i] = {
        idRow: tableData2[i].row.idRow,
        client: {
          label: tableData2Labels[i].row.client as string,
          value: tableData2[i].row.client as string,
        },
        organizationUnit: {
          label: tableData2Labels[i].row.organizationUnit as string,
          value: tableData2[i].row.organizationUnit as string,
        },
        nomenclature: {
          label: tableData2Labels[i].row.nomenclature as string,
          value: tableData2[i].row.nomenclature as string,
        },
        nomenclatureCharacteristic: {
          label: tableData2Labels[i].row.nomenclatureCharacteristic as string,
          value: tableData2[i].row.nomenclatureCharacteristic as string,
        },
        unitOfMeasurement: {
          label: tableData2Labels[i].row.unitOfMeasurement as string,
          value: tableData2[i].row.unitOfMeasurement as string,
        },
        productType: {
          label: tableData2Labels[i].row.productType as string,
          value: tableData2[i].row.productType as string,
        },
        reportedYear: {
          label: isActionCopy ? '' : tableData2Labels[i].row.reportedYear as string,
          value: isActionCopy ? '' : tableData2[i].row.reportedYear as string,
        },
        reportedMonth: {
          label: isActionCopy ? '' : tableData2Labels[i].row.reportedMonth as string,
          value: isActionCopy ? '' : tableData2[i].row.reportedMonth as string,
        },
        sum: isActionCopy ? '' : tableData2[i].row.sum as string,
        sumInKzt: '',
        amount: tableData2[i].row.amount as string,
        vatRate: {
          label: tableData2Labels[i].row.vatRate as string,
          value: tableData2[i].row.vatRate as string,
        },
        budgetItem: {
          label: tableData2Labels[i].row.budgetItem as string,
          value: tableData2[i].row.budgetItem as string,
        },
        application4SpendingGuid: '',
      }
    }

    // setState
    this.setState({
      db3state: {
        datablock: 3,
        tableData: resultingTableData,
        options: {
          clients: {
            available: [],
            isLoading: true
          },
          organizationUnits: {
            available: [],
            isLoading: false
          },
          nomenclatures: {
            available: [],
            isLoading: true
          },
          nomenclatureCharacteristics: {
            available: new Map<String, Array<any>>(),
            isLoading: false
          },
          unitOfMeasurements: {
            available: [],
            isLoading: true
          },
          budgetItems: {
            available: [],
            isLoading: true
          },
        }
      }
    });
  }

  prefillBlock4 = (customFields: POCustomFields) => {
    this.setState({
      db4state: {
        budgetYear: customFields.budgetYear?.value as MyOptionInterface,
        providerContacts: customFields.providerContactData?.value as string,
        description: customFields.descriptionCF?.value as string,
        region: {
          available: customFields.regionAvailableOptions?.value as MyOptionInterface[],
          isLoading: false,
          value: customFields.region?.value as MyOptionInterface
        },
      }
    });
  }
}



export default CreateIssueForm;