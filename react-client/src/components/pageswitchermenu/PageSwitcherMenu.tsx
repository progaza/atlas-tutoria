import React from 'react';
import Button from '@atlaskit/button';
import { Group } from '@atlaskit/navigation-next';
import PoButton from '../common/PoButton';


const PageSwitcherMenu = (props) => {
  function handleClick(e) {
    props.onPickOfTheForm(e.currentTarget.id);
  }

  function onSubmitClickHandler() {
    props.onSubmitClickHandler();
  }

  const selectedButton = props.selectedButton;
  return (
    <div>
      <Group heading="Блоки данных" hasSeparator>
        <Button isSelected={selectedButton === 'datablock1'} id='datablock1' shouldFitContainer onClick={handleClick}>Блок №1</Button>
        <Button isSelected={selectedButton === 'datablock2'} id='datablock2' shouldFitContainer onClick={handleClick}>Блок №2</Button>
        <Button isSelected={selectedButton === 'datablock3'} id='datablock3' shouldFitContainer onClick={handleClick}>Блок №3</Button>
        <Button isSelected={selectedButton === 'datablock4'} id='datablock4' shouldFitContainer onClick={handleClick}>Блок №4</Button>
        <PoButton appearance="primary" id='submitButtonId' shouldFitContainer onSubmitClickHandler={onSubmitClickHandler} inputText="Отправить" />
      </Group>
    </div>
  )
}

export default PageSwitcherMenu;