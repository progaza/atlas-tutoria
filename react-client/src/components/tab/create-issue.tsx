import React, {Component} from 'react';
import AppStateInterface from "../../models/AppStateInterface";
import Form, {Field, FormFooter} from "@atlaskit/form"
import Textfield from "@atlaskit/textfield";
import {Button} from "@atlaskit/button/dist/es2019/components/Button";
import TextArea from "@atlaskit/textarea";
import Select, {OptionType} from "@atlaskit/select";
import PoSelectList from "../common/PoSelectList";
import MyOptionInterface from "../../models/MyOptionInterface";

export class CreateIssue extends Component<{}, AppStateInterface> {
    values = {
        name: '',
        content: ''
    };

    constructor(props) {
        super(props);
        this.state = {
            //@ts-ignore
            name: '',
            content: '',
            receiver: {}
        }
    };


    render() {

        return (
            <Form
                onSubmit={data => {
                    console.log('data: ', data);
                    alert('el==s');
                }}>
                {({formProps}) => (
                    <form {...formProps} name="text-fields" style={styles.formStyle}>
                        <Field name="name" defaultValue="" label="Issue name" isRequired>
                            {({fieldProps}) => <Textfield {...fieldProps} />}
                        </Field>

                        <Field name="content" defaultValue="" label="Content" isRequired>
                            {({fieldProps: {isRequired, isDisabled, ...others}}) => (
                                <Textfield
                                    disabled={isDisabled}
                                    required={isRequired}
                                    {...others}
                                />
                            )}
                        </Field>

                        <Field name="receiver" label="Select receiver" isRequired>
                            {({fieldProps: {isRequired, isDisabled, onChange, value, ...others}}) => (
                                <Select<OptionType>
                                    options={this.props.receiverOptions}
                                    onChange={onValue => {this.setState({ receiver: onValue }); console.log('onValue: ', this.state)}}
                                    // onInputChange={inputValue => {this.setState({receiver: inputValue});console.log('inputChange: ', this.state)}}
                                    disabled={isDisabled}
                                    required={isRequired}
                                    {...others}
                                />
                            )}

                        </Field>
                        <FormFooter>
                            <Button type="submit" appearance="primary">
                                Submit
                            </Button>
                        </FormFooter>
                    </form>
                )}
            </Form>
        );
    }

    contractChooseHandler = (v) => {
        console.log('selected: ', v);
        this.setState(
            //@ts-ignore
            {receiver: v});
        console.log('this.state: ', this.state)
    }

    /* region a
    render() {
        return (<div>
                <h1>Create issue</h1>
                <form onSubmit={(val) => {
                    console.log('hello: ', val.target);
                    console.log('1values: ', this.values);
                    alert('hello');
                }}>
                    <div >
                        <Textfield isReadOnly={false} name="name" />
                        <Textfield isReadOnly={false} name="content" value={this.values.content}/>
                        <Button onClick={() => {
                            console.log('values: ', this.values);
                            console.log('clicked');
                        }} type='submit'>Hello</Button>
                    </div>
                </form>
            </div>
        );
    }
    endregion */
}

const
    styles = {
        formStyle: {
            width: '100%'
        } as React.CSSProperties
    }