import React, { Component, CSSProperties } from 'react'
import Button from '@atlaskit/button';

interface IProps {
    onSubmitClickHandler?: any,
    appearance?: "default" | "link" | "danger" | "primary" | "subtle" | "subtle-link" | "warning" | undefined,
    id: string,
    shouldFitContainer?: boolean,
    inputText: string,
    type?: string;
    style?: CSSProperties | undefined;
    secondsToSpin?: number;
}

interface IState {
    isLoading: boolean,
}

class PoButton extends Component<IProps, IState> {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false
        }

        this.onSubmitClickHandler = this.onSubmitClickHandler.bind(this);
    }

    onSubmitClickHandler() {
        this.setState({
            isLoading: true
        });

        this.props.onSubmitClickHandler();

        const { secondsToSpin } = this.props;

        setTimeout(() => {
            this.setState({
                isLoading: false
            });
        }, (secondsToSpin) ? (secondsToSpin * 1000) : 5000);
    }

    render() {
        const { appearance, id, shouldFitContainer, inputText, type, style } = this.props;

        return (
            <Button appearance={appearance} id={id} type={type} shouldFitContainer={shouldFitContainer} isLoading={this.state.isLoading} onClick={this.onSubmitClickHandler} style={style}>{inputText}</Button>
        );
    }
}


export default PoButton;