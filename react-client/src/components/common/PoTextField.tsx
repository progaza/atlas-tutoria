import React, { Fragment } from 'react'
import Textfield from '@atlaskit/textfield';
import { Field, ErrorMessage, ValidMessage, HelperMessage } from '@atlaskit/form';
// import { validateTextField, validateNumberTextField } from '../../core/utils/fields-validator';

interface IProps {
    id: string;
    name: string;
    label?: string;
    className?: string;
    defaultValue?: string;
    placeholder?: string;
    isRequired?: boolean;
    isReadOnly?: boolean;
    isDisabled?: boolean;
    onBlur?: any;
    onChange?: any;
    helperMessage?: string;
    textFieldType?: string;
    value?: string
}

const PoTextField = (
    { id, name, label,
        className,
        defaultValue,
        placeholder,
        isRequired = false,
        isReadOnly,
        isDisabled,
        onBlur,
        onChange,
        helperMessage,
        value,
        textFieldType
    }: IProps,) => {

    // function validateFunc(value,
    //     formState,
    //     fieldState) {
    //     if (textFieldType === 'number') return validateNumberTextField(value);
    //     else return validateTextField(value);
    // }

    // var isValueEmpty = false;

    const isTextTypeNumber = Boolean(textFieldType === 'number');

    return (
        <div className={className} >
            <Field
                id={id}
                name={name}
                label={label}
                defaultValue={defaultValue}
                isDisabled={isDisabled}
                isRequired={isRequired}
            // validate={(value,
            //     formState,
            //     fieldState) => {
            //     if (isRequired) {
            //         const res = validateFunc(value,
            //             formState,
            //             fieldState);
            //         return res;
            //     }

            //     if (!value) isValueEmpty = true;
            //     return;
            // }}
            >
                {({ error, valid }) => (
                    <Fragment>
                        <div>
                            <Textfield
                                id={id}
                                name={name}
                                label={label}
                                defaultValue={defaultValue}
                                isDisabled={isDisabled}
                                isRequired={isRequired}
                                css
                                placeholder={placeholder}
                                isReadOnly={isReadOnly}
                                onBlur={onBlur}
                                onChange={onChange}
                                value={value}
                            />
                        </div>
                        {!error && !valid && (
                            <HelperMessage>
                                {helperMessage}
                            </HelperMessage>
                        )}
                        {!(isRequired && !defaultValue) && (isDisabled === false) && (isReadOnly === undefined) && (
                            (
                                isTextTypeNumber &&
                                // (defaultValue) &&
                                !Boolean(Number(defaultValue))
                                // Boolean(defaultValue.search(/ |\./g) > -1)
                                // !Boolean(Number(defaultValue?.replace(/,/g, "."))
                            ) ?
                                <ErrorMessage>{'Используйте точку или запятую только для указания дробных значений'}</ErrorMessage>
                                : <ValidMessage>Отлично!</ValidMessage>
                        )}
                        {/* {(isRequired && !defaultValue) && <ErrorMessage>{error}</ErrorMessage> } */}
                        {(isRequired && !defaultValue) && <ErrorMessage>{'Поля обязательно к заполнению'}</ErrorMessage>}
                    </Fragment>
                )}
            </Field>
        </div>
    );
}

export default PoTextField;