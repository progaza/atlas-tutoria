import React from 'react';
import Spinner from '@atlaskit/spinner';

const LoadingScreen = () => {
    return (<div style={{
        display: 'flex',
        justifyContent: 'center',
        marginTop: '150px',
        marginBottom: '150px'
    }}>
        <Spinner size='large' />
    </div>);
}

export default LoadingScreen