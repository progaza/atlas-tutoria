import React, { Fragment } from 'react'
import TextArea from '@atlaskit/textarea';
import { Field, ErrorMessage, ValidMessage } from '@atlaskit/form';

const PoTextField = ({ id, name, label = '', isDisabled = false, defaultValue = '', isRequired = false, placeholder = '', isReadOnly = false, onChange = (value) => { }, onBlur = (value) => { }, minimumRows = 3, spellCheck = true },) => {
    return (
        <Field
            id={id}
            name={name}
            label={label}
            isRequired={isRequired}
        >
            {({ error }) => (
                <Fragment>
                    {<TextArea minimumRows={minimumRows} resize='auto' spellCheck={spellCheck} isDisabled={isDisabled} isReadOnly={isReadOnly} defaultValue={defaultValue} placeholder={placeholder} onBlur={onBlur} onChange={onChange} />}
                    {/* {error && <ErrorMessage>{error}</ErrorMessage>} */}
                    {(isRequired) && (defaultValue) && (isDisabled === false) && (isReadOnly === false) && (
                        <ValidMessage>
                            Отлично!
                        </ValidMessage>
                    )}
                    {(isRequired && !defaultValue) && <ErrorMessage>{'Поле обязательно к заполнению'}</ErrorMessage>}
                </Fragment>
            )}
        </Field>
    );
}

export default PoTextField;