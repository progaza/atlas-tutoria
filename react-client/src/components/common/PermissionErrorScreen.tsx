import React from 'react';

const PermissionErrorScreen = () => {
    return (<div className='app-spinner'>
    <div className='alert'>
      <b><u>Внимание!</u></b><br /><br />У Вас нет доступа на работу с данным запросом!
      </div>
  </div>);
}

export default PermissionErrorScreen