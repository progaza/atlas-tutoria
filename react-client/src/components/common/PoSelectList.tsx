import React, {Fragment} from 'react'
import Select, {OptionType, ValueType} from '@atlaskit/select';
import {Field, ErrorMessage, HelperMessage} from '@atlaskit/form';
import {ValidationState} from '@atlaskit/select/types';

const PoSelectList = (
    {
        name, label, isRequired, options,
        helperMsg = '',
        defaultInputValue = '',
        defaultValue = null,
        placeholder = '',
        className = '',
        errorText = '',
        validationState = '' as ValidationState,
        value = {label: '', value: ''} as OptionType,
        isLoading = false,
        isDisabled = false,
        onChange = (value) => {
        },
        onBlur = (value) => {
        },
        onInputChange = (value) => {
        }
    }) => {


    // check and set is value empty
    var isValueEmpty = false;
    if (!value) isValueEmpty = true;
    if (value.value === '') {
        isValueEmpty = true;
        value = {label: placeholder, value: 'none'};
    }

    // check and set validatation state according to value and disabled, required flags
    var validationStateValue;
    if (validationState) validationStateValue = validationState;
    else if (isDisabled === true || !isRequired) {
        validationStateValue = 'default';
    } else {
        validationStateValue = (isValueEmpty) ? 'error' : 'success'
    }

    // if options are undefined, then field can't be required and state can't be 'error'
    if (!options || !options.length) {
        validationStateValue = 'default';
        isRequired = false;
        // isDisabled = true;
    }

    return (
        <Field<ValueType<OptionType>>
            name={name}
            label={label}
            isRequired={isRequired}
        >
            {({fieldProps: {id, ...rest}, error}) => (
                <Fragment>
                    <Select<OptionType>
                        inputId={id}
                        {...rest}
                        options={options}
                        placeholder={placeholder}
                        className={"single-select myTestForm " + className}
                        classNamePrefix="react-select"
                        value={value}
                        defaultValue={defaultValue}
                        defaultInputValue={defaultInputValue}
                        onChange={onChange}
                        onInputChange={onInputChange}
                        onBlur={onBlur}
                        isLoading={isLoading}
                        isDisabled={isDisabled}
                        validationState={validationStateValue}
                    />
                    <HelperMessage>{helperMsg}</HelperMessage>

                    {(validationState === 'error' || error) && <ErrorMessage>{errorText}</ErrorMessage>}
                </Fragment>
            )}
        </Field>
    );
}

export default PoSelectList;