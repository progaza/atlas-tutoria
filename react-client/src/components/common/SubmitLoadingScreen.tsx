import React from 'react';
import Spinner from '@atlaskit/spinner';
import EmojiEmojiIcon from '@atlaskit/icon/glyph/emoji/emoji';

const SubmitLoadingScreen = () => {
    return (<div className='app-spinner'>
        <div className='info'>
            <b><u>Загрузка!</u></b><br />
            <div style={{
                display: 'flex',
                justifyContent: 'center'
            }}><Spinner size='large' /></div><br />
            <br />Создаются заявки в 1С и в JIRA, ожидайте, пожалуйста <EmojiEmojiIcon label='smile' size='medium' primaryColor='yellowgreen' />
        </div>
    </div>);
}

export default SubmitLoadingScreen