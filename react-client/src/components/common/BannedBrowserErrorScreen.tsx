import React from 'react';
import './BannedBrowserErrorScreen.css';

const BannedBrowserErrorScreen = () => {
    return (<div className="ban-ie-content">
        <h1>Создание заявок в системе JIRA через браузер Internet Explorer не доступно.</h1>
        <p>Пожалуйста, используйте любой другой браузер например Chrome/Opera/Edge/Firefox</p>
        <footer className="ban-ie-content__footer">
            <span>Спасибо за понимание, С уважением команда BO</span>
        </footer>
    </div>);
}

export default BannedBrowserErrorScreen