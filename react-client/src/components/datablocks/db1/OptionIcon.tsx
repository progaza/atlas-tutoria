import React, { Component } from 'react'
import PriorityBlockerIcon from '@atlaskit/icon-priority/glyph/priority-blocker';
import PriorityHighIcon from '@atlaskit/icon-priority/glyph/priority-high';
import PriorityMediumIcon from '@atlaskit/icon-priority/glyph/priority-medium';
import PriorityLowIcon from '@atlaskit/icon-priority/glyph/priority-low';

class Optionicon extends Component<{ optionValue: any }, {}> {
    render() {
        let optionValue = this.props.optionValue;
        return (
            <div>
                {optionValue === '10100' &&
                    <PriorityBlockerIcon label="Critical" size="small" />
                }
                {optionValue === '2' &&
                    <PriorityHighIcon label="High" size="small" />
                }
                {optionValue === '3' &&
                    <PriorityMediumIcon label="Medium" size="small" />
                }
                {optionValue === '10000' &&
                    <PriorityLowIcon label="Low" size="small" />
                }
            </div>
        );
    }
}

export default Optionicon;