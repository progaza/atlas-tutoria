import React, { Component, Fragment } from 'react'
import Select, { FormatOptionLabelMeta, OptionType, ValueType } from '@atlaskit/select';
import { Field, ErrorMessage } from '@atlaskit/form';
import PoTextField from '../../common/PoTextField';
import Form, { FormHeader, FormSection, FormFooter } from '@atlaskit/form';
import { getAjsMeta, getIssueAction, getIssueKey } from "../../../core/utils/jira.utils";
import ProjectConstants from '../../../core/constants/ProjectConstants'
import PoSelectList from '../../common/PoSelectList';
import Optionicon from './OptionIcon';
import Db1stateInterface from '../../../models/Db1stateInterface';
import { getApiCall } from '../../../core/api/api';
import ProviderSearchField from '../../providersearchfield/ProviderSearchField';
import MyOptionInterface from '../../../models/MyOptionInterface';

const { urls, issueActionTypes } = ProjectConstants

interface IProps {
  passState: any;
  passCurrencyData: any;
  passApplicationForSpendingTableData: any;
  curState?: Db1stateInterface;
}

class DataBlock1 extends Component<IProps, Db1stateInterface> {
  constructor(props) {
    super(props)

    this.state = {
      datablock: 1,
      username: props?.curState?.username,
      fullName: props?.curState?.fullName,
      organization: props?.curState?.organization,
      department: props?.curState?.department,
      provider: {
        availableOptions: [],
        linkedContracts: [],
        name: '',
        isLoading: false,
        isValid: false,
      },
      priority: { label: 'Low', value: '10000' },
      contractsOptions: [],
      application4Spending: {
        availableOptions: [],
        name: '',
        value: '',
        isLoading: false,
        providerForApplications: '',
        tableData: [],
      },
      isLoading: true,
      isPrefilled: false,
      issueKey: ''
    }

    this.contractChooseHandler = this.contractChooseHandler.bind(this);
    this.providerChangeHandler = this.providerChangeHandler.bind(this);
    this.providerInputChangeHandler = this.providerInputChangeHandler.bind(this);
    this.initApplication4SpendingsOptions = this.initApplication4SpendingsOptions.bind(this);
    this.onApplicationForSpendingChangeHandler = this.onApplicationForSpendingChangeHandler.bind(this);
    this.getApplicationForSpendingTableData = this.getApplicationForSpendingTableData.bind(this);
  }

  componentDidMount() {
    const { curState } = this.props;
    // if applicant wasn't initialized then search him
    if (!curState?.fullName) {
      // get data about applicant
      this.getApplicantData();
    }

    this.setState({
      isLoading: false
    });

    const issueKey = getIssueKey();
    if (issueKey) {
      this.setState({
        issueKey: issueKey
      });
    }

    const issueAction = getIssueAction();
    if (issueAction) {
      this.setState({
        issueAction: issueAction
      });
    }
  }

  componentWillUnmount() {
    this.props.passState(this.state);
  }

  render() {
    const isCurStateEmpty = !Boolean(this.props.curState?.provider);
    const isPrefilled = this.state.isPrefilled

    const issueAction = this.state.issueAction;

    const finalCnd = (!isCurStateEmpty && !isPrefilled);
    if (finalCnd) {
      const { provider, contractsOptions, chosenContract, application4Spending, priority } = this.props.curState as Db1stateInterface;
      this.setState({
        // provider: provider,
        contractsOptions: contractsOptions,
        chosenContract: chosenContract,
        application4Spending: application4Spending,
        priority: priority,
        isPrefilled: true,
        isLoading: true
      });

      // fetch data from service about this application for spending
      setTimeout(() => {
        // this.getApplicationForSpendingTableData(application4Spending?.value, chosenContract?.value);
        this.props.passCurrencyData(chosenContract?.currency);

        this.initContractsData(provider?.chosen?.chosenProvider.value, provider);

        if (Boolean(application4Spending?.value) && !Boolean(application4Spending?.name)) this.initChosenApplication4SpendingsOptionAndSetIt(chosenContract?.value as string, application4Spending?.value as string);
        this.setState({
          isLoading: false
        })
      }, 0);
    }

    const provider = this.state.provider;
    const contractsOptions = this.state.contractsOptions;
    const chosenContract = this.state.chosenContract;
    const application4Spending = this.state.application4Spending;
    const priority = this.state.priority;

    const contractNumValue = (chosenContract) ? { label: chosenContract?.label, value: chosenContract?.value } : { label: '', value: '' } as OptionType;
    const contractCurrencyValue = (chosenContract) ? { label: chosenContract?.currency, value: chosenContract?.currency } : {} as OptionType;
    const chosenContractSum = (chosenContract?.sum) ? chosenContract?.sum : '';
    const chosenContractProcurementType = (chosenContract?.procurementType) ? chosenContract?.procurementType : '';
    const application4SpendingValue: OptionType = (application4Spending?.value) ? { label: application4Spending?.name, value: application4Spending?.value } : { label: '', value: '' };

    const isIssueKeyFilled = Boolean(this.state.issueKey);

    return (
      <div>
        <Form onSubmit={() => console.log}>
          {({ formProps }) => (
            <form action='' id='db1' {...formProps}>
              <FormHeader title={"Блок № " + this.state.datablock} />
              <FormSection title="Данные заявителя">
                <div className='db1-applicant-data'>
                  <PoTextField id='fullName' name='fullName' label='ФИО' isReadOnly={true} placeholder={this.state.fullName} className='db1-sec1-fields' />
                  <PoTextField id='organization' name='organization' label='Организация' isReadOnly={true} placeholder={this.state.organization} className='db1-sec1-fields' />
                  <PoTextField id='department' name='department' label='Департамент' isReadOnly={true} placeholder={this.state.department} className='db1-sec1-fields' />
                </div>
              </FormSection>
              <FormSection title="Данные по договору">
                <div className='db1-contract-data'>
                  <div>
                    <ProviderSearchField isDisabled={(issueAction === issueActionTypes.update && isIssueKeyFilled)} curState={provider?.chosen?.chosenProvider} passProvider={(provider) => {
                      this.providerChangeHandler(provider);
                    }} />
                  </div>
                  {provider?.chosen?.chosenProvider &&
                    <div>
                      <PoSelectList
                        name='contractNum'
                        label='Номер контракта'
                        isRequired={true}
                        options={contractsOptions}
                        placeholder='Выберите договор'
                        value={contractNumValue}
                        onChange={this.contractChooseHandler}
                        isLoading={provider.isLoading}
                        isDisabled={(issueAction === issueActionTypes.update && isIssueKeyFilled)}
                      />
                    </div>
                  }
                  {chosenContract?.value &&
                    <div>
                      <PoSelectList
                        name='applicationForSpending'
                        label='Заявка на расходования средств'
                        isRequired={false}
                        options={application4Spending?.availableOptions}
                        isLoading={application4Spending?.isLoading}
                        placeholder='Выберите заявку'
                        value={application4SpendingValue}
                        onChange={value => this.onApplicationForSpendingChangeHandler(value)}
                        isDisabled={(issueAction === issueActionTypes.update && isIssueKeyFilled)}
                      />
                    </div>
                  }
                  {chosenContract?.value &&
                    <div>
                      <PoSelectList
                        name='currency'
                        label='Валюта'
                        isRequired={!(Boolean(chosenContract?.currency))}
                        isDisabled={(Boolean(chosenContract?.currency))}
                        options={currencyOptions}
                        placeholder='Выберите валюту'
                        value={contractCurrencyValue}
                        onChange={(option) => {
                          let updatedChosenContract = chosenContract;
                          updatedChosenContract.currency = option.label
                          this.setState({
                            chosenContract: updatedChosenContract
                          });
                          this.props.passCurrencyData(option.label);
                        }}
                      />
                    </div>
                  }
                  {chosenContract?.value &&
                    <div>
                      <PoTextField id='amountOfContract' name='amountOfContract' label='Сумма договора' isReadOnly={true} value={(chosenContractSum) ? chosenContractSum : '0'} />
                    </div>
                  }
                  {chosenContract?.value &&
                    <div>
                      <PoTextField id='procurementType' name='procurementType' label='Тип закупа' isReadOnly={true} value={chosenContractProcurementType} />
                    </div>
                  }
                </div>

                <div className="po_db1_priority"><Field<ValueType<OptionType>>
                  name="priority"
                  label="Приоритет"
                >
                  {({ fieldProps: { id, ...rest }, error }) => (
                    <Fragment>
                      <Select<OptionType>
                        inputId={id}
                        onChange={value => {
                          this.props.passState(this.state);
                          return this.setState({ priority: value })
                        }}
                        value={priority}
                        options={priorityOptions}
                        placeholder="Выберите приоритет"
                        className="single-select"
                        classNamePrefix="react-select"
                        formatOptionLabel={formatOptionLabel}
                      // validationState={(!this.state?.priority) ? 'error' : 'success'}
                      />
                      {error && <ErrorMessage>{error}</ErrorMessage>}
                    </Fragment>
                  )}
                </Field>
                </div>
              </FormSection>
              <FormFooter>
              </FormFooter>
            </form>
          )}
        </Form>
      </div >
    );
  }

  initApplication4SpendingsOptions(contractGuid: string) {
    const application4Spending = this.state.application4Spending;

    const isInitializedApplicationForSpending = this.getIsInitializedApplicationForSpending(application4Spending, contractGuid);

    if (!isInitializedApplicationForSpending) {
      let updatedApplication4Spending = this.state.application4Spending;

      // set loading flag
      if (updatedApplication4Spending) {
        updatedApplication4Spending.isLoading = true;

        this.setState({
          application4Spending: updatedApplication4Spending
        });
      } else {
        this.setState({
          application4Spending: {
            isLoading: true,
            name: '',
            value: '',
          }
        });
      }

      // init applicationForSpending
      getApiCall(urls.getApplicationForSpending + '?q=' + contractGuid)
        .then(data => {
          if (!data) throw Error('data is empty');

          const applicationForSpendingOptions = this.convertApplicationDataToOptions(data);

          if (updatedApplication4Spending) {
            updatedApplication4Spending.availableOptions = applicationForSpendingOptions;
            updatedApplication4Spending.isLoading = false;
            updatedApplication4Spending.providerForApplications = contractGuid;

            this.setState({
              application4Spending: updatedApplication4Spending
            });
          } else {
            this.setState({
              application4Spending: {
                availableOptions: applicationForSpendingOptions,
                isLoading: false,
                providerForApplications: contractGuid,
                name: '',
                value: '',
              }
            });
          }

        })
        .catch((ex) => { console.log('Error fetch/catch', ex); });
    }
  }

  initChosenApplication4SpendingsOptionAndSetIt(contractGuid: string, application4SpendingGuid: string) {
    // set loading flag
    let updatedApplication4Spending = this.state.application4Spending;
    if (updatedApplication4Spending)
      updatedApplication4Spending.isLoading = true;
    this.setState({
      application4Spending: updatedApplication4Spending
    });

    // get chosen application 4 spending
    getApiCall(urls.getApplicationForSpending + '?q=' + contractGuid + '&v=' + application4SpendingGuid)
      .then(data => {
        if (!data) throw Error('data is empty');

        const applicationForSpendingOptions = this.convertApplicationDataToOptions([data]);

        if (updatedApplication4Spending) {
          updatedApplication4Spending.availableOptions = applicationForSpendingOptions;
          updatedApplication4Spending.isLoading = false;
          updatedApplication4Spending.providerForApplications = contractGuid;

          // set chosen value for application4spending
          updatedApplication4Spending.name = applicationForSpendingOptions[0].label
          updatedApplication4Spending.value = applicationForSpendingOptions[0].value

          this.setState({
            application4Spending: updatedApplication4Spending,
          });
        } else {
          this.setState({
            application4Spending: {
              availableOptions: applicationForSpendingOptions,
              isLoading: false,
              providerForApplications: contractGuid,
              name: '',
              value: '',
            }
          });
        }
      })
      .catch((ex) => { console.log('Error fetch/catch', ex); });
  }

  getIsInitializedApplicationForSpending(application4Spending, providerId) {
    let result = Boolean(application4Spending?.availableOptions?.length) && Boolean(providerId === application4Spending?.providerForApplications)

    return result
  }

  convertApplicationDataToOptions(data): MyOptionInterface[] {
    let result: Array<any> = [];
    data.forEach(item => {
      result.push({
        label: `Заявка № ${item?.number} | Сумма ${item?.sum} | Дата расхода ${item?.date}`,
        value: item?.guid
      });
    });
    return result;
  }

  getApplicantData() {
    const currentUser = getAjsMeta("remote-user");
    console.log('currentUser', currentUser);

    // get data from external system using asynchronous request
    getApiCall(urls.getUserData + '?q=' + currentUser)
      .then(data => {
        if (!data) throw Error('data is empty');

        console.log('getApplicantData', data);

        this.setState({
          fullName: data?.fullName,
          organization: 'ДБ АО "Хоум Кредит"',
          department: data?.department,
          username: currentUser
        });

        return data;
      })
      .catch((ex) => { console.error('Error fetch/catch', ex); });
  }

  contractChooseHandler(value) {
    // update state according to information from chosenContract   
    this.setState({
      chosenContract: this.getDataForChosenContract(value)
    });

    this.initApplication4SpendingsOptions(value?.value);

    this.onApplicationForSpendingChangeHandler(undefined);
    this.props.passApplicationForSpendingTableData(null);

    setTimeout(() => { this.props.passState(this.state) }, 0);
  }

  getDataForChosenContract(chosenContract) {
    let result
    // get data from external system according to BIN
    const linkedContracts = this.state.provider?.linkedContracts;
    const foundContract = linkedContracts?.find((item) => {
      if (item?.value === chosenContract?.value) {
        return item;
      }
      return null;
    });

    if (chosenContract) {
      result = foundContract

      // pass curency to parent component
      this.props.passCurrencyData(foundContract?.currency);
    } else {
      result = undefined;
    }

    return result;
  }

  providerChangeHandler(chosenProvider) {
    let updatedProvider = this.state.provider

    if (chosenProvider && updatedProvider) {
      updatedProvider.chosen = { chosenProvider }
    }

    this.setState({
      provider: updatedProvider
    });

    this.contractChooseHandler(undefined);
    // this.props.passProviderId(chosenProvider?.value);
    this.providerInputChangeHandler(chosenProvider.value);

    this.props.passState(this.state);
  }

  parseProviderData(data) {
    const providerData = {
      name: data[0].providerName,
    }

    let linkedContracts: Array<any> = [];

    data.forEach(item => {
      let obj = {
        label: item?.label,
        value: item?.value,
        currency: item?.contractCurrency,
        sum: item?.contractSum,
        procurementType: item?.contractProcurType
      }
      linkedContracts.push(obj);
    })

    return { providerData: providerData, linkedContracts: linkedContracts }
  }

  providerInputChangeHandler(value: string) {
    if (!value) return;

    value = value.trim();
    let updatedProvider = this.state.provider;
    if (updatedProvider)
      updatedProvider.isLoading = true;

    this.setState({ provider: updatedProvider });


    // when input of bin is finished get data from ext system
    // get data about this provider - his name also get contracts linked to this provider - name and guid
    // and show available option
    // const isBinValid = value.length === 12;
    const isBinValid = value.length > 3;

    if (isBinValid) {
      this.initContractsData(value, updatedProvider);
    } else {
      if (updatedProvider)
        updatedProvider.isValid = isBinValid

      this.setState({
        provider: updatedProvider
      });
    }
  }

  initContractsData = (binValue, updatedProvider) => {
    getApiCall(urls.getProviderAndContractData + '?q=' + binValue)
      .then(data => {
        if (!data.length) throw Error('data is empty');

        // have to parse data in providerName + providerGuid and LinkedContracts
        const { providerData, linkedContracts } = this.parseProviderData(data);

        let availableProviderOption = [{ label: providerData.name, value: binValue }]
        if (updatedProvider) {
          updatedProvider.availableOptions = availableProviderOption;
          updatedProvider.linkedContracts = linkedContracts;
          updatedProvider.isValid = true;
          updatedProvider.isLoading = false;

          const newContractsOptions = linkedContracts.map((item) => {
            return { label: item?.label, value: item.value };
          });

          this.setState({
            provider: updatedProvider,
            contractsOptions: newContractsOptions,
          });
        } else {
          const newContractsOptions = linkedContracts.map((item) => {
            return { label: item?.label, value: item.value };
          });

          this.setState({
            provider: {
              availableOptions: availableProviderOption,
              linkedContracts: linkedContracts,
              isValid: true,
              isLoading: false,
              name: '',
            },
            contractsOptions: newContractsOptions
          });
        }

        return data;
      })
      .catch((ex) => {
        alert('Ошибка! По данному поставщику не было найдено ни 1 договора. Обратитесь в бухгалтерию или заведите инцидент');
        console.error('Error fetch/catch', ex);
        this.setState({
          provider: {
            availableOptions: [],
            linkedContracts: [],
            isValid: true,
            isLoading: false,
            name: '',
          },
          contractsOptions: []
        });
      });
  }

  onApplicationForSpendingChangeHandler(value) {
    if (!value) {
      const updatedApplication4Spending = this.state.application4Spending;
      if (updatedApplication4Spending) {
        updatedApplication4Spending.name = '';
        updatedApplication4Spending.value = '';
      }

      this.setState({
        application4Spending: updatedApplication4Spending
      });

      this.props.passApplicationForSpendingTableData(null);
    } else {
      const updatedApplication4Spending = this.state.application4Spending;

      if (updatedApplication4Spending) {
        updatedApplication4Spending.name = value.label;
        updatedApplication4Spending.value = value.value;
      }

      this.setState({
        application4Spending: updatedApplication4Spending
      });

      // fetch data from service about this application for spending
      this.getApplicationForSpendingTableData(value.value, null);
    }

    this.props.passState(this.state);
  }

  getApplicationForSpendingTableData(applicationGuid, contractGuid) {
    // get provider guid
    if (!contractGuid) contractGuid = this.state?.chosenContract?.value;

    if (!contractGuid) {
      console.error('getApplicationForSpendingTableData', 'contractGuid is empty', contractGuid);
      return null;
    }

    if (!applicationGuid) {
      console.error('getApplicationForSpendingTableData', 'applicationGuid is empty', applicationGuid);
      return null;
    }

    // fetch data
    getApiCall(urls.getApplicationForSpending + '?q=' + contractGuid + '&v=' + applicationGuid)
      .then(data => {
        if (!data) {
          this.props.passApplicationForSpendingTableData(null);
          throw Error('data is empty');
        }

        const tableData = data?.tableData?.map((row) => JSON.parse(row));

        const updatedApplication4Spending = this.state.application4Spending;

        if (updatedApplication4Spending) {
          updatedApplication4Spending.tableData = tableData;
        }

        this.setState({
          application4Spending: updatedApplication4Spending
        });

        // pass data 
        // convert map to array
        if (tableData.length) {
          this.props.passApplicationForSpendingTableData(tableData);
        } else {
          this.props.passApplicationForSpendingTableData([tableData]);
        }
      })
      .catch((ex) => { console.error('Error fetch/catch', ex); });
  }

  addApplicationGuid2TableData(tableData, applicationGuid) {
    let map: Map<String, any> = new Map(Object.entries(tableData));
    map.set('application4SpendingGuid', applicationGuid);
    return Object.fromEntries(map)
  }
}

const formatOptionLabel = (
  option: OptionType,
  { context }: FormatOptionLabelMeta<OptionType>,
) => {
  if (context === 'menu') {
    return (
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <Optionicon optionValue={option.value} />
        <span
          style={{
            paddingLeft: 8,
            paddingBottom: 0,
          }}
        >
          {option.label}
        </span>
      </div>
    );
  }
  return option.label;
};

const currencyOptions = [
  { label: 'KZT', value: 'kzt' },
  { label: 'USD', value: 'usd' },
  { label: 'EUR', value: 'eur' },
  { label: 'RUB', value: 'rub' }
];

const priorityOptions = [
  { label: 'Critical', value: '10100' },
  { label: 'High', value: '2' },
  { label: 'Medium', value: '3' },
  { label: 'Low', value: '10000' }
];

export default DataBlock1;