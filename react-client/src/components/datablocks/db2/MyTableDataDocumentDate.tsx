import React, { Component, Fragment } from "react";
import { Field, ErrorMessage } from "@atlaskit/form";
import { DatePicker } from "@atlaskit/datetime-picker";
// import { getCurrentDate } from "../../../core/utils/jira.utils";

class MyTableDataDocumentDate extends Component<{ idRow: any; value?: any; passValue: any; }, {}> {
    constructor(props) {
        super(props);

        this.onDocumentDataChange = this.onDocumentDataChange.bind(this);
    }

    onDocumentDataChange(value) {
        const data = {
            id: this.props.idRow,
            value: value,
            type: 'date'
        };
        this.props.passValue(data);
    }

    render() {
        const { idRow, value } = this.props;
        // const currentDate = getCurrentDate();

        return (
            <td>
                <Field
                    name="documentDate"
                    id={"documentDate" + idRow}
                    isRequired
                >
                    {({ fieldProps: { id }, error }) => (
                        <Fragment>
                            <DatePicker value={value} id={id} onChange={this.onDocumentDataChange} />
                            {error && <ErrorMessage>{error}</ErrorMessage>}
                        </Fragment>
                    )}
                </Field>
            </td>
        );
    }
}



export default MyTableDataDocumentDate;