import React from 'react';
import MyOptionInterface from "../../../models/MyOptionInterface";
import { Button } from "@atlaskit/button/dist/cjs/components/Button";
import MyTableDataDocumentType from "./MyTableDataDocumentType";
import MyTableDataDocumentNum from "./MyTableDataDocumentNum";
import MyTableDataDocumentDate from "./MyTableDataDocumentDate";
import TrashIcon from '@atlaskit/icon/glyph/trash';

const MyTableRow = ({ idRow, documentType, documentNum, documentDate, removeRowClick, passValueHandler, showDeleteRowIcon }: {
    idRow: number, documentType: MyOptionInterface | string, documentNum: string, documentDate: string, removeRowClick: any, passValueHandler: Function, showDeleteRowIcon: boolean
}) => {
    return (
        <tr id={idRow.toString()} className='db2-table-columns'>
            <MyTableDataDocumentType value={documentType} idRow={idRow} passValue={passValueHandler} />
            <MyTableDataDocumentNum value={documentNum} idRow={idRow} passValue={passValueHandler} />
            <MyTableDataDocumentDate value={documentDate} idRow={idRow} passValue={passValueHandler} />
            {(showDeleteRowIcon) &&
                <td className='db2-table-deleteRow'><Button id={idRow.toString()} appearance='link' onClick={removeRowClick}><TrashIcon label='delete' /></Button></td>
            }
        </tr>
    );
}

export default MyTableRow;