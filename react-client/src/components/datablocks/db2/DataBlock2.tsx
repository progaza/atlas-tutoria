import React, { Component } from 'react'
import MyTable from './MyTable';
import Form, { FormHeader, FormSection, FormFooter } from '@atlaskit/form';
import Db2stateInterface from '../../../models/Db2stateInterface';
import ErrorBoundary from '../../common/ErrorBoundary';

interface IProps {
  passState: any;
  curState?: Db2stateInterface;
}

class DataBlock2 extends Component<IProps, Db2stateInterface> {
  constructor(props) {
    super(props)

    this.state = {
      datablock: 2,
      tableData: props?.curState?.tableData,
    }

    this.passState = this.passState.bind(this);
  }

  passState(data) {
    this.props.passState({
      datablock: 2,
      tableData: data?.tableData
    });
  }

  render() {
    return (
      <div>
        <Form onSubmit={data => console.log('form data', data)}>
          {({ formProps }) => (
            <form action='' id='db2' {...formProps}>
              <FormHeader title={"Блок № " + this.state.datablock} />
              <FormSection title="Информация по счетам">
                <ErrorBoundary>
                  <MyTable curState={this.state.tableData} passState={this.passState} />
                </ErrorBoundary>
              </FormSection>
              <FormFooter>
              </FormFooter>
            </form>
          )}
        </Form>
      </div >
    );
  }
}

export default DataBlock2;