import React, { Component, Fragment } from "react";
import MyOptionInterface from "../../../models/MyOptionInterface";
import { Field, ErrorMessage } from "@atlaskit/form";
import Select, { OptionType, ValueType } from '@atlaskit/select';

export const documentTypeOptions: MyOptionInterface[] = [
    { label: 'Счет', value: 'bill' },
    { label: 'Счет-фактура', value: 'invoice' },
    { label: 'Другое', value: 'other' }
];

class MyTableDataDocumentType extends Component<{ idRow: any; value: MyOptionInterface | string; passValue: any; }, {}> {
    constructor(props) {
        super(props);

        this.onDocumentDataChange = this.onDocumentDataChange.bind(this);
    }

    onDocumentDataChange(value) {
        const data = {
            id: this.props.idRow,
            value: value,
            type: 'type'
        };

        this.props.passValue(data);
    }

    render() {
        const { idRow } = this.props;
        let { value } = this.props;
        let convertedValue: MyOptionInterface | undefined;
        if (typeof (value) === 'string') {
            convertedValue = documentTypeOptions.find(item => {
                return (item.value === value)
            });
        } else {
            convertedValue = value;
        }

        return (
            <td>
                <Field<ValueType<OptionType>>
                    name="documentType"
                    id={"documentType" + idRow}
                    isRequired
                >
                    {({ fieldProps: { id }, error }) => (
                        <Fragment>
                            <Select<OptionType>
                                value={convertedValue}
                                inputId={id}
                                options={documentTypeOptions}
                                placeholder="Укажите тип документа"
                                className="single-select"
                                classNamePrefix="react-select"
                                onChange={this.onDocumentDataChange}
                            />
                            {error && <ErrorMessage>{error}</ErrorMessage>}
                        </Fragment>
                    )}
                </Field>
            </td >
        );
    }
}

export default MyTableDataDocumentType;