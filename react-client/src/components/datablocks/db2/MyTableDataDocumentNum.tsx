import React, { Component } from "react";
import PoTextField from "../../common/PoTextField";

class MyTableDataDocumentNum extends Component<{ idRow: any; value: string; passValue: any; }, {}> {
    constructor(props) {
        super(props);

        this.onDocumentDataChange = this.onDocumentDataChange.bind(this);
    }

    onDocumentDataChange(event) {
        const data = {
            id: this.props.idRow,
            value: event.target.value,
            type: 'number'
        };

        this.props.passValue(data);
    }

    render() {
        const { idRow, value } = this.props;

        return (
            <td>
                <PoTextField
                    id={"documentNum" + idRow}
                    defaultValue={value}
                    name='documentNum'
                    isRequired={false}
                    placeholder='Укажите номер документа'
                    onBlur={this.onDocumentDataChange} />
            </td>
        );
    }
}

export default MyTableDataDocumentNum;