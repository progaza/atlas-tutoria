import React, { Component } from 'react'
import Button from '@atlaskit/button';
import AddCircleIcon from '@atlaskit/icon/glyph/add-circle';
import Db2stateInterface, { Db2MyTableStateInterface } from '../../../models/Db2stateInterface';
import MyTableRow from './MyTableRow';


interface IProps {
    passState: any;
    curState: Db2MyTableStateInterface[];
}

class MyTable extends Component<IProps, Db2stateInterface> {
    constructor(props) {
        super(props);

        const isCurStateEmpty = Boolean(props.curState?.length) && !Boolean(props.curState[0].documentNum);

        this.state = {
            datablock: 2,
            tableData: (isCurStateEmpty) ? [{ idRow: 1, documentType: '', documentNum: '', documentDate: '' }] : props.curState,
            isPrefilled: false,
        }

        this.addRowClickHandler = this.addRowClickHandler.bind(this);
        this.removeRowClickHandler = this.removeRowClickHandler.bind(this);
        this.passValueHandler = this.passValueHandler.bind(this);
    }

    componentWillUnmount() {
        this.props.passState(this.state);
    }

    render() {
        const tableData = this.state.tableData;
        return (
            <div>
                <table className='db2-table'>
                    <thead>
                        <tr className='db2-table-columns'>
                            <th>Тип документа</th>
                            <th>Номер документа</th>
                            <th>Дата документа</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            tableData.map((row, index) => {
                                console.log('row', row);
                                return <MyTableRow
                                    key={row.idRow}
                                    idRow={row.idRow}
                                    documentType={row?.documentType}
                                    documentNum={row?.documentNum}
                                    documentDate={row?.documentDate}
                                    removeRowClick={this.removeRowClickHandler}
                                    passValueHandler={this.passValueHandler}
                                    showDeleteRowIcon={index !== 0}
                                />
                            })
                        }
                    </tbody>
                </table>
                <div className='db2-table-addRow'>
                    <div></div><div></div><div></div>
                    <div id='addRowBtn'>
                        <Button appearance='link' onClick={this.addRowClickHandler}> <AddCircleIcon label='addRow' size='large' /> </Button>
                    </div>
                </div>
            </div>
        );
    }

    addRowClickHandler() {
        let lastRow = this.state.tableData[this.state.tableData.length - 1]
        let newLastRowId = lastRow?.idRow + 1;

        let tableData = this.state.tableData;
        let newRowObject = { idRow: newLastRowId, documentType: '', documentNum: '', documentDate: '' };
        if (tableData) {
            tableData.push(newRowObject);
        } else {
            tableData = [newRowObject]
        }

        this.setState({ tableData: tableData });
    }

    removeRowClickHandler(event) {
        const idRow2Delete = event.currentTarget.id;
        const tableData = this.state.tableData;

        if (idRow2Delete !== 1) {
            const updatedTableData = tableData.filter(item => item.idRow !== Number(idRow2Delete))

            this.setState({
                tableData: updatedTableData
            });
        }
    }

    passValueHandler = (data) => {
        const updatedTD = this.state.tableData.map(item => {
            if (item.idRow !== data.id) return item;
            else {
                if (data.type === 'number') {
                    item.documentNum = data.value;
                } else if (data.type === 'date') {
                    item.documentDate = data.value;
                } else if (data.type === 'type') {
                    item.documentType = data.value;
                } else {
                    throw new Error('something went wrong');
                }
                return item;
            }
        });

        this.setState({ tableData: updatedTD });
    }
}

export default MyTable;