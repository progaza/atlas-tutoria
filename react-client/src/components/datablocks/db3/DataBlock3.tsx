import React, { Component } from 'react';
import Form, { FormHeader, FormSection, FormFooter } from '@atlaskit/form';
import Button from '@atlaskit/button';
import PoSelectList from '../../common/PoSelectList';
import PoTextField from '../../common/PoTextField';
import ProjectConstants from '../../../core/constants/ProjectConstants'
import TrashIcon from '@atlaskit/icon/glyph/trash';
import CopyIcon from '@atlaskit/icon/glyph/copy';
import AddCircleIcon from '@atlaskit/icon/glyph/add-circle';
import ErrorBoundary from '../../common/ErrorBoundary';
import Db3stateInterface, { Db3MyTableStateInterface } from '../../../models/Db3stateInterface';
import MyOptionInterface from '../../../models/MyOptionInterface';
import { compareOptionsByLabel, getIssueKey } from '../../../core/utils/jira.utils';
import { getApiCall } from '../../../core/api/api';
import TotalSumRow from './TotalSumRow';


const { urls } = ProjectConstants

interface IProps {
    passState: any;
    curState: any;
    dataFromOtherBlocks: any;
}

class DataBlock3 extends Component<IProps, Db3stateInterface> {
    constructor(props) {
        super(props);

        // get data from props
        const tableDataFromProps = props.curState?.tableData;
        const appl4SpendingFromProps = (tableDataFromProps) ? tableDataFromProps[0]?.application4SpendingGuid : undefined;

        // get data from block1
        const tableDataFromOtherBlock = props?.dataFromOtherBlocks?.tableData;
        const appl4SpendingFromOtherBlock = (tableDataFromOtherBlock) ? tableDataFromOtherBlock[0]?.application4SpendingGuid : undefined;

        // resulted table data to set to state
        let resultedTableData = [{
            idRow: 0,
            client: '',
            organizationUnit: '',
            nomenclature: '',
            nomenclatureCharacteristic: '',
            unitOfMeasurement: '',
            productType: '',
            reportedYear: '',
            reportedMonth: '',
            sum: '',
            sumInKzt: '',
            amount: '',
            vatRate: '',
            budgetItem: '',
            application4SpendingGuid: '',
        }];

        const isEmptyTableDataFromOtherBlock = !Boolean(tableDataFromOtherBlock?.length)
        const isEmptyTableDataFromProps = !Boolean(tableDataFromProps?.length)
        if (isEmptyTableDataFromOtherBlock) {
            // console.log('case1');
            if (!isEmptyTableDataFromProps) {
                // console.log('case11');
                resultedTableData = tableDataFromProps;
            }
        } else {
            // console.log('case2');
            if (!isEmptyTableDataFromProps && (appl4SpendingFromProps === appl4SpendingFromOtherBlock || appl4SpendingFromProps === '')) {
                // console.log('case22');
                resultedTableData = tableDataFromProps;
            }
            else {
                // console.log('case23');
                let idRowVal = 0;
                const checkIfTableDataHasEmptyArray = !(tableDataFromOtherBlock?.length && tableDataFromOtherBlock[0] instanceof Array && tableDataFromOtherBlock?.length);

                if (checkIfTableDataHasEmptyArray) {
                    resultedTableData = this.formatTableDataFromOtherBlock2Resulted(tableDataFromOtherBlock, idRowVal);
                }
            }
        }
        // console.log('case4');

        this.state = {
            datablock: 3,
            tableData: resultedTableData,
            options: (props.curState?.options?.clients?.available?.length) ? props.curState?.options : {
                clients: {
                    available: [],
                    isLoading: true
                },
                organizationUnits: {
                    available: [],
                    isLoading: false
                },
                nomenclatures: {
                    available: [],
                    isLoading: true
                },
                nomenclatureCharacteristics: {
                    available: new Map<String, Array<any>>(),
                    isLoading: false
                },
                unitOfMeasurements: {
                    available: [],
                    isLoading: true
                },
                budgetItems: {
                    available: [],
                    isLoading: true
                }
            },
            currencyData: props?.dataFromOtherBlocks?.currencyData,
            isPrefilled: false,
        }

        this.addRowClickHandler = this.addRowClickHandler.bind(this);
        this.removeRowClickHandler = this.removeRowClickHandler.bind(this);
        this.initAvailableOptions = this.initAvailableOptions.bind(this);
        this.initAvailableCharacteristics = this.initAvailableCharacteristics.bind(this);
        this.initAvailableOrganizationUnits = this.initAvailableOrganizationUnits.bind(this);
        this.calculateAmountInKzt = this.calculateAmountInKzt.bind(this);
        this.checkAndInitSecondaryFields = this.checkAndInitSecondaryFields.bind(this);
        this.getCurrencyData = this.getCurrencyData.bind(this);
    }

    render() {
        const tableData = this.state.tableData;
        const options = this.state.options;

        const isToUpdate = Boolean(this.state.isPrefilled);

        let totalSumValue = 0
        let totalSumInKztValue = 0

        tableData.forEach(row => {
            totalSumValue = totalSumValue + Number(row.sum);
            totalSumInKztValue = totalSumInKztValue + Number(row.sumInKzt);
        });

        const { totalSum, totalSumInKzt } = this.state;

        // console.log('totalSumValue is ', totalSumValue, 'totalSum is', totalSum, 'cnd is ', Boolean(totalSumValue !== totalSum));
        // console.log('totalSumInKztValue is ', totalSumInKztValue, 'totalSumInKzt is', totalSumInKzt, 'cnd is ', Boolean(totalSumInKztValue !== totalSumInKzt));
        if (((!isNaN(totalSumValue as number) && !isNaN(totalSum as number)) && totalSumValue !== totalSum) || totalSumInKztValue !== totalSumInKzt) {
            this.setState({
                totalSum: totalSumValue,
                totalSumInKzt: totalSumInKztValue
            });
        }

        console.log('render tableData is', tableData);

        return (
            <div>
                <Form onSubmit={data => console.log('form data', data)}>
                    {({ formProps }) => (
                        <form action='' id='db3' {...formProps}>
                            <FormHeader title={"Блок № " + this.state.datablock} />
                            <FormSection title="Детали">
                                <ErrorBoundary>
                                    {
                                        tableData?.map(row => {
                                            const { idRow, client, organizationUnit, nomenclature, nomenclatureCharacteristic, productType, reportedYear, reportedMonth, sum, sumInKzt, amount, vatRate, budgetItem, application4SpendingGuid } = row
                                            let { unitOfMeasurement } = row
                                            const optionsNomenCharacAvailable = options?.nomenclatureCharacteristics?.available?.get((nomenclature as MyOptionInterface)?.value);

                                            // if measurementUnit is empty and linked to nomenclature has value, then get them
                                            if (!(unitOfMeasurement as MyOptionInterface)?.value &&
                                                options?.nomenclatures?.nomenAndUnitsLink) {
                                                unitOfMeasurement = this.getMeasurementUnitAccordingToNomenclature(nomenclature as MyOptionInterface)
                                            }

                                            return <fieldset key={'row' + idRow}>
                                                <div><h2># {Number(idRow) + 1}</h2></div>
                                                <div className="field-group db3-content">
                                                    <PoSelectList
                                                        name='client'
                                                        label='Клиент'
                                                        isRequired={true}
                                                        options={options?.clients?.available}
                                                        placeholder='Выберите клиента'
                                                        value={client as MyOptionInterface}
                                                        onChange={value => this.onChangeHandler(value, idRow, 'client')}
                                                        isLoading={options?.clients?.isLoading}
                                                        isDisabled={!isToUpdate && Boolean(application4SpendingGuid && client)}
                                                    />
                                                    <PoSelectList
                                                        name='organizationUnit'
                                                        label='Подразделение организации'
                                                        isRequired={true}
                                                        options={options?.organizationUnits?.available}
                                                        isLoading={options?.organizationUnits?.isLoading}
                                                        placeholder='Выберите подразделение организации'
                                                        value={organizationUnit as MyOptionInterface}
                                                        onChange={value => this.onChangeHandler(value, idRow, 'organizationUnit')}
                                                        isDisabled={!isToUpdate && Boolean(application4SpendingGuid && organizationUnit)}
                                                    />
                                                    <PoSelectList
                                                        name='productType'
                                                        label='Тип продукта'
                                                        isRequired={false}
                                                        options={productTypeOptions}
                                                        placeholder='Выберите тип продукта'
                                                        value={productType as MyOptionInterface}
                                                        onChange={value => this.onChangeHandler(value, idRow, 'productType')}
                                                        isDisabled={!isToUpdate && Boolean(application4SpendingGuid && productType)}
                                                    />
                                                    <PoSelectList
                                                        name='nomenclature'
                                                        label='Наименование ТРУ'
                                                        isRequired={true}
                                                        options={options?.nomenclatures?.available}
                                                        isLoading={options?.nomenclatures?.isLoading}
                                                        placeholder='Выберите ТРУ'
                                                        value={nomenclature as MyOptionInterface}
                                                        onChange={value => this.onChangeHandler(value, idRow, 'nomenclature')}
                                                        isDisabled={!isToUpdate && Boolean(application4SpendingGuid && nomenclature)}
                                                    />
                                                    <PoSelectList
                                                        name='nomenclatureCharacteristic'
                                                        label='Характеристика ТРУ'
                                                        isRequired={false}
                                                        options={optionsNomenCharacAvailable}
                                                        isLoading={options?.nomenclatureCharacteristics?.isLoading}
                                                        placeholder='Выберите характеристику ТРУ'
                                                        value={nomenclatureCharacteristic as MyOptionInterface}
                                                        onChange={value => this.onChangeHandler(value, idRow, 'nomenclatureCharacteristic')}
                                                        isDisabled={!isToUpdate && Boolean(application4SpendingGuid && nomenclatureCharacteristic)}
                                                    />
                                                    <PoSelectList
                                                        name='unitOfMeasurement'
                                                        label='Единица измерения'
                                                        isRequired={false}
                                                        isDisabled={true}
                                                        options={options?.unitOfMeasurements?.available}
                                                        isLoading={options?.unitOfMeasurements?.isLoading}
                                                        placeholder='Выберите единицу измерения'
                                                        value={unitOfMeasurement as MyOptionInterface}
                                                        onChange={value => this.onChangeHandler(value, idRow, 'unitOfMeasurement')}
                                                    />
                                                    <PoSelectList
                                                        name='reportedYear'
                                                        label='Отчетный год'
                                                        isRequired={true}
                                                        options={yearsOptions}
                                                        placeholder='Выберите отчётный год'
                                                        value={reportedYear as MyOptionInterface}
                                                        onChange={value => this.onChangeHandler(value, idRow, 'reportedYear')}
                                                        isDisabled={!isToUpdate && Boolean(application4SpendingGuid && reportedYear)}
                                                    />
                                                    <PoSelectList
                                                        name='reportedMonth'
                                                        label='Отчетный месяц'
                                                        isRequired={true}
                                                        options={monthsOptions}
                                                        placeholder='Выберите отчётный месяц'
                                                        value={reportedMonth as MyOptionInterface}
                                                        onChange={value => this.onChangeHandler(value, idRow, 'reportedMonth')}
                                                        isDisabled={!isToUpdate && Boolean(application4SpendingGuid && reportedMonth)}
                                                    />
                                                    <PoSelectList
                                                        name='vatRate'
                                                        label='Ставка НДС'
                                                        isRequired={true}
                                                        options={vatRateOptions}
                                                        placeholder='Выберите ставку НДС'
                                                        value={vatRate as MyOptionInterface}
                                                        onChange={value => this.onChangeHandler(value, idRow, 'vatRate')}
                                                        isDisabled={!isToUpdate && Boolean(application4SpendingGuid && vatRate)}
                                                    />
                                                    <PoTextField
                                                        id='amount'
                                                        name='amount'
                                                        placeholder='Укажите количество'
                                                        label='Количество'
                                                        isRequired={true}
                                                        defaultValue={amount?.toString()}
                                                        onBlur={value => this.onChangeHandler(value, idRow, 'amount')}
                                                        isDisabled={!isToUpdate && Boolean(application4SpendingGuid && amount)}
                                                        textFieldType='number'
                                                    />
                                                    <PoTextField
                                                        id='sum'
                                                        name='sum'
                                                        label='Сумма'
                                                        isRequired={true}
                                                        placeholder='Укажите сумму'
                                                        defaultValue={sum?.toString()}
                                                        onBlur={value => this.onChangeHandler(value, idRow, 'sum')}
                                                        isDisabled={!isToUpdate && Boolean(application4SpendingGuid && sum)}
                                                        textFieldType='number'
                                                    />
                                                    <PoTextField
                                                        id='sumInKzt'
                                                        name='sumInKzt'
                                                        label='Эквивалент в KZT'
                                                        isDisabled={true}
                                                        placeholder={(sumInKzt) ? sumInKzt?.toString() : 'Будет расчитан автоматически'}
                                                        onBlur={value => this.onChangeHandler(value, idRow, 'sumInKzt')}
                                                    />
                                                    <PoSelectList
                                                        name='budgetItem'
                                                        label='Статья бюджета'
                                                        isRequired={true}
                                                        options={options?.budgetItems?.available}
                                                        isLoading={options?.budgetItems?.isLoading}
                                                        placeholder='Выберите статью бюджета'
                                                        value={budgetItem as MyOptionInterface}
                                                        onChange={value => this.onChangeHandler(value, idRow, 'budgetItem')}
                                                        isDisabled={!isToUpdate && Boolean(application4SpendingGuid && budgetItem)}
                                                    />
                                                    <div></div>

                                                    <div className='db3-content-deleteRowBtn'>
                                                        {(idRow > 0 && !application4SpendingGuid) &&
                                                            <Button id={idRow as string} appearance='link' onClick={e => this.removeRowClickHandler(e, idRow)}><TrashIcon label='Удалить'></TrashIcon></Button>
                                                        }
                                                        <Button id={idRow as string} appearance='link' onClick={e => this.copyRowClickHandler(e, idRow)}><CopyIcon label='Копировать'></CopyIcon></Button>
                                                    </div>

                                                </div>
                                                {idRow >= 0 && ((idRow as number) + 1) !== tableData.length &&
                                                    <hr />
                                                }
                                            </fieldset>
                                        })
                                    }
                                    <div className='db3-table-addRow'>
                                        <div></div><div></div>
                                        <div id='addRowBtn'>
                                            <Button appearance='link' onClick={this.addRowClickHandler}> <AddCircleIcon label='addRow' size='large' /> </Button>
                                        </div>
                                    </div>
                                </ErrorBoundary>
                            </FormSection>
                            <FormFooter>
                            </FormFooter>
                        </form>
                    )}
                </Form>
                <TotalSumRow totalSum={totalSumValue} totalSumInKzt={totalSumInKztValue} />
            </div >
        );
    }

    componentWillUnmount() {
        this.props.passState(this.state);
    }

    componentDidMount() {
        this.initAvailableOptions();

        this.checkAndInitSecondaryFields();

        // added for validation
        if (getIssueKey()) {
            this.setState({
                isPrefilled: true,
                budgetItemWasChanged: false,
            });
        }
    }

    onChangeHandler(data, rowIndex, type) {
        // idRow = idRow - 1;

        const tableData = this.state.tableData;
        let updatedTableData: Db3MyTableStateInterface[] = JSON.parse(JSON.stringify(tableData));
        let row2Update = updatedTableData.find(row => row.idRow === rowIndex) as Db3MyTableStateInterface
        let numberValue;

        switch (type) {
            case 'client':
                row2Update.client = data;

                // clear current value of organizationUnit
                row2Update.organizationUnit = '';

                // init options for ogranization units
                this.initAvailableOrganizationUnits(data);

                break;
            case 'organizationUnit':
                row2Update.organizationUnit = data;
                break;
            case 'nomenclature':
                row2Update.nomenclature = data;

                // clear current value of characteristics of nomenclatures
                row2Update.nomenclatureCharacteristic = '';

                // init options for characteristics of nomenclatures
                this.initAvailableCharacteristics(data);

                // and set unit according to nomenclature
                row2Update.unitOfMeasurement = this.getMeasurementUnitAccordingToNomenclature(data);

                // and set budgetItem according to nomenclature
                row2Update.budgetItem = this.getBudgetItemAccordingToNomenclature(data);
                break;
            case 'nomenclatureCharacteristic':
                row2Update.nomenclatureCharacteristic = data;
                break;
            case 'unitOfMeasurement':
                row2Update.unitOfMeasurement = data;
                break;
            case 'productType':
                row2Update.productType = data;
                break;
            case 'reportedYear':
                row2Update.reportedYear = data;
                break;
            case 'reportedMonth':
                row2Update.reportedMonth = data;
                break;
            case 'sum':
                numberValue = data.target.value;
                // numberValue = Number(numberValue?.replace(/,/g, "."));
                // console.log('numberValue', numberValue, 'Number(numberValue)', Number(numberValue))

                if(!numberValue) {
                    numberValue = 0
                } else if (Number(numberValue)){
                    numberValue = Number(numberValue)
                } else if (Number(numberValue.replace(/,/g, '.'))) {
                    numberValue = Number(numberValue.replace(/,/g, '.'));
                }
                row2Update.sum = numberValue;
                this.calculateAmountInKzt(rowIndex, numberValue);

                console.log('sum, row2Update is ', row2Update);
                break;
            case 'sumInKzt':
                row2Update.sumInKzt = data.target.value;
                break;
            case 'amount':
                numberValue = data.target.value;
                numberValue = Number(numberValue?.replace(/,/g, "."));
                console.log('numberValue', numberValue, 'Number(numberValue)', Number(numberValue))

                row2Update.amount = numberValue;
                break;
            case 'vatRate':
                row2Update.vatRate = data;
                break;
            case 'budgetItem':
                row2Update.budgetItem = data;
                break;
            default:
                console.error('default', data, rowIndex, type)
                break;
        }

        // added for validation
        if (type === 'budgetItem' && this.state.isPrefilled) {
            this.setState({
                tableData: updatedTableData,
                budgetItemWasChanged: true,
            }, () => this.props.passState(this.state));
        } else {
            if (type === 'sum') return;
            else {
                this.setState({
                    tableData: updatedTableData
                }, () => this.props.passState(this.state));
            }
        }
    }

    addRowClickHandler() {
        let lastRow = this.state.tableData[this.state.tableData.length - 1]
        let newLastRowId = (lastRow?.idRow as number) + 1;

        let tableData = this.state.tableData;
        let newRowObject = {
            idRow: newLastRowId,
            client: '',
            organizationUnit: '',
            nomenclature: '',
            nomenclatureCharacteristic: '',
            unitOfMeasurement: '',
            productType: '',
            reportedYear: '',
            reportedMonth: '',
            sum: '',
            sumInKzt: '',
            amount: '',
            vatRate: '',
            budgetItem: '',
            application4SpendingGuid: '',
        };
        if (tableData) {
            tableData.push(newRowObject);
        } else {
            tableData = [newRowObject]
        }

        this.setState({ tableData: tableData });
        this.props.passState(this.state);
    }

    async removeRowClickHandler(e, idRow) {
        const tableData = this.state.tableData;

        if (idRow !== 0) {
            const updatedTableData = tableData.filter(item => item.idRow !== Number(idRow));
            await this.setState({ tableData: updatedTableData });
            this.props.passState(this.state);
        }
    }

    async copyRowClickHandler(e, idRow) {
        const tableData = this.state.tableData;
        let lastRow = this.state.tableData[this.state.tableData.length - 1]
        let newLastRowId = (lastRow?.idRow as number) + 1;

        const row2Copy = (tableData.find(item => item.idRow === Number(idRow)) as Db3MyTableStateInterface);
        let copiedRow = JSON.parse(JSON.stringify(row2Copy));
        copiedRow.idRow = newLastRowId;
        const updatedTableData = JSON.parse(JSON.stringify(tableData));;
        updatedTableData.push(copiedRow);
        await this.setState({ tableData: updatedTableData });
        this.props.passState(this.state);
    }

    initAvailableOptions() {
        const options = this.state.options;
        const isInitializedClients = (options?.clients?.available?.length);
        const isInitializedNomenclatures = (options?.nomenclatures?.available?.length);
        const isInitializedMeasurementUnits = (options?.unitOfMeasurements?.available?.length);
        const isInitializedBudgetItems = (options?.budgetItems?.available?.length);

        if (!isInitializedClients) {
            // init clientOptions
            getApiCall(urls.getDataFrom1C + '?q=departments')
                .then(data => {
                    if (!data) throw Error('data is empty')
                    let updatedOptions = this.state.options;

                    updatedOptions.clients = {
                        available: data,
                        isLoading: false
                    }

                    this.setState({
                        options: updatedOptions
                    });
                    return data;
                })
                .catch((ex) => { console.error('Error fetch/catch', ex); });
        }

        if (!isInitializedNomenclatures) {

            // init nomenclatures
            getApiCall(urls.getDataFrom1C + '?q=nomenclatures')
                .then(data => {
                    if (!data) throw Error('data is empty')
                    let updatedOptions = this.state.options;

                    const availableNomenOptions: [{ label: string, value: string }] = data.map((item) => {
                        return { label: item.name, value: item.guid };
                    });

                    const nomenclatureAndUnitsLink = data.map((item) => {
                        return { nomenId: item.guid, unitId: item.measurementUnit };
                    });

                    const nomenAndBudgetItemLink = data.map((item) => {
                        return { nomenId: item.guid, budgetItemLabel: item.budgetItemName };
                    });

                    updatedOptions.nomenclatures = {
                        available: availableNomenOptions.sort(compareOptionsByLabel),
                        isLoading: false,
                        nomenAndUnitsLink: nomenclatureAndUnitsLink,
                        nomenAndBudgetItemLink: nomenAndBudgetItemLink,
                    }

                    this.setState({
                        options: updatedOptions
                    });

                    return data;
                })
                .catch((ex) => { console.error('Error fetch/catch', ex); });
        }

        if (!isInitializedMeasurementUnits) {
            // init measurementUnits
            getApiCall(urls.getDataFrom1C + '?q=measurementUnits')
                .then(data => {
                    if (!data) throw Error('data is empty')
                    let updatedOptions = this.state.options;

                    updatedOptions.unitOfMeasurements = {
                        available: data,
                        isLoading: false
                    }

                    this.setState({
                        options: updatedOptions
                    });
                    return data;
                })
                .catch((ex) => { console.error('Error fetch/catch', ex); });
        }

        if (!isInitializedBudgetItems) {

            // init budgetItems
            getApiCall(urls.getDataFrom1C + '?q=budgetItems')
                .then(data => {
                    if (!data) throw Error('data is empty');
                    let updatedOptions = this.state.options;

                    updatedOptions.budgetItems = {
                        available: data.sort(compareOptionsByLabel),
                        isLoading: false
                    }

                    this.setState({
                        options: updatedOptions
                    });
                    return data;
                })
                .catch((ex) => { console.error('Error fetch/catch', ex); });
        }
    }

    checkAndInitSecondaryFields() {
        // check each row of table data
        const tableData = this.state.tableData;
        const optionsNC = this.state.options.nomenclatureCharacteristics

        // if totalInKzt null, and sum and amount are known, then calculate total
        tableData.forEach((row) => {
            const nomenclatureObj = row.nomenclature as MyOptionInterface;

            if ((!row.sumInKzt) && (row.sum)) {
                this.calculateAmountInKzt(row?.idRow, Number(row.sum));
            }

            // if charceristicsOptions empty, and nomenclature is known, then get them
            if (nomenclatureObj) {
                const mapOfOptions: Map<String, Array<MyOptionInterface>> = optionsNC?.available;
                const areOptionsInitialized = Boolean(Object.fromEntries(mapOfOptions)[nomenclatureObj.value]);

                if (!areOptionsInitialized) {
                    this.initAvailableCharacteristics(nomenclatureObj)
                }
            }
        });
    }

    initAvailableCharacteristics(nomenclatureOption: MyOptionInterface) {
        // set loading
        let updatedOptions = this.state.options;
        updatedOptions.nomenclatureCharacteristics = {
            available: updatedOptions.nomenclatureCharacteristics?.available,
            isLoading: true
        };

        this.setState({
            options: updatedOptions
        });

        getApiCall(urls.getDataFrom1C + '?q=nomenclatureCharacteristics&v=' + nomenclatureOption.value)
            .then(data => {
                if (!data) throw Error('data is empty')

                // get current map from state
                // check if map contains this key
                // then do nothing
                // else add new pair to map


                // get current map from state
                let mapOfNomenAndCharacOptions: Map<String, Array<any>> = updatedOptions.nomenclatureCharacteristics.available; // map on nomenclature guid and nomenclature characterisics options
                // check if map contains this key
                if (mapOfNomenAndCharacOptions.has(nomenclatureOption?.value)) {
                    // then do nothing
                } else {
                    // else add new pair to map
                    mapOfNomenAndCharacOptions.set(nomenclatureOption?.value, data);
                }

                updatedOptions.nomenclatureCharacteristics = {
                    available: mapOfNomenAndCharacOptions,
                    isLoading: false
                };

                this.setState({
                    options: updatedOptions
                });
                return data;
            })
            .catch((ex) => { console.error('Error fetch/catch', ex); });
    }

    getMeasurementUnitAccordingToNomenclature = (nomenclatureOption: MyOptionInterface) => {
        // get id of measurementUnit
        const nomenAndUnitsLink = this.state.options.nomenclatures.nomenAndUnitsLink
        const unitId = nomenAndUnitsLink?.find((item) => {
            return (item.nomenId === nomenclatureOption.value)
        })?.unitId

        // find option according to id
        const resOption = (this.state.options.unitOfMeasurements.available as MyOptionInterface[]).find((item) => {
            return (item.value === unitId)
        });

        return resOption
    }

    getBudgetItemAccordingToNomenclature(inputData) {
        // get id of measurementUnit
        const nomenAndBudgetItemLink = this.state.options.nomenclatures.nomenAndBudgetItemLink
        const budgetItemLabel = nomenAndBudgetItemLink?.find((item) => {
            return (item.nomenId === inputData.value)
        })?.budgetItemLabel

        // find option according to id
        const resOption = (this.state.options.budgetItems.available as MyOptionInterface[]).find((item) => {
            return (item.label === budgetItemLabel)
        });

        return resOption
    }

    formatTableDataFromOtherBlock2Resulted(tableDataFromOtherBlock, idRowVal) {
        // idRowVal = idRowVal - 1;
        return tableDataFromOtherBlock.map((row) => {
            const budgetYearVal = yearsOptions.find((option) => option.value === row?.budgetYear);
            const budgetMonthVal = monthsOptions.find((option) => option.value === row?.budgetMonth);
            const productTypeVal = productTypeOptions.find((option) => option.label === row?.productType);
            let vatRateVal;
            const vatRateFromRow = row?.NDSrate;

            if (vatRateFromRow.indexOf('12') !== -1) vatRateVal = vatRateOptions[1];
            else if (vatRateFromRow.indexOf('0') !== -1) vatRateVal = vatRateOptions[0];
            else vatRateVal = vatRateOptions[2];

            // idRowVal = idRowVal + 1;
            // console.log('idRowVal to set is ', idRowVal);

            const nomenclatureVal = JSON.parse(row?.item) as MyOptionInterface
            const unitOfMeasurementOption = JSON.parse(row?.measurementUnit) as MyOptionInterface
            const unitOfMeasurementVal = (unitOfMeasurementOption.value) ? unitOfMeasurementOption : { label: '', value: '' }

            console.log('nomenclatureVal is ', nomenclatureVal, 'unitOfMeasurementOption', unitOfMeasurementOption, 'unitOfMeasurementVal', unitOfMeasurementVal);

            return {
                idRow: idRowVal++,
                client: JSON.parse(row?.client),
                organizationUnit: JSON.parse(row?.region),
                nomenclature: nomenclatureVal,
                nomenclatureCharacteristic: JSON.parse(row?.itemCharacteristics),
                unitOfMeasurement: unitOfMeasurementVal,
                productType: productTypeVal,
                reportedYear: budgetYearVal,
                reportedMonth: budgetMonthVal,
                sum: row?.amount,
                sumInKzt: '',
                amount: row?.quantity,
                vatRate: vatRateVal,
                budgetItem: JSON.parse(row?.budgetArticle),
                application4SpendingGuid: row?.application4SpendingGuid,
            }
        });
    }

    initAvailableOrganizationUnits(inputData) {
        // set loading
        let updatedOptions = this.state.options;
        updatedOptions.organizationUnits = {
            available: [],
            isLoading: true
        };

        this.setState({
            options: updatedOptions
        });

        getApiCall(urls.getDataFrom1C + '?q=organizationUnits&v=' + inputData?.value)
            .then(data => {
                if (!data) throw Error('data is empty')

                updatedOptions.organizationUnits = {
                    available: data,
                    isLoading: false
                };

                this.setState({
                    options: updatedOptions
                });
                return data;
            })
            .catch((ex) => { console.error('Error fetch/catch', ex); });
    }

    async calculateAmountInKzt(rowIndex, sum: number) {
        const currencyCode = this.props?.dataFromOtherBlocks?.currencyData;
        const currentCurrencyData = this.state.currencyData;

        const currencyData = (currentCurrencyData?.guid) ? currentCurrencyData : await this.getCurrencyData(currencyCode);

        const rate = Number(currencyData?.rate);

        const tableData = this.state.tableData;
        let updatedTableData: Db3MyTableStateInterface[] = JSON.parse(JSON.stringify(tableData));
        let row2Update = updatedTableData.find(row => Number(row.idRow) === Number(rowIndex)) as Db3MyTableStateInterface

        const sumInKzt = sum * rate

        row2Update.sum = sum;
        row2Update.sumInKzt = (sumInKzt) ? sumInKzt : 0;
        console.log('before setState updatedTableData is ', updatedTableData);
        console.log('row2Update is', row2Update);

        this.setState({
            tableData: updatedTableData
        }, () => { this.props.passState(this.state); });
    }

    getCurrencyData(currencyCode) {
        return getApiCall(urls.getDataFrom1C + '?q=currency&v=' + currencyCode)
            .then(data => {
                if (!data) throw Error('data is empty')

                this.setState({
                    currencyData: data
                });
                return data;
            })
            .catch((ex) => { console.error('Error fetch/catch', ex); });
    }
}

const productTypeOptions = [
    { label: 'Кредиты', value: 'be026d96-bdce-11e5-ad2a-0050569e61e6' },
    { label: 'Депозиты', value: 'b02c72bd-bf4d-11e5-a860-0050569e61e6' },
    // { label: 'Карты', value: '05f3ad41-bde4-11e5-ad2a-0050569e61e6' },
    { label: 'Кредитные карты', value: '231b54fd-bf51-11e5-a860-0050569e61e6' },
    { label: 'Дебетные карты', value: '5b436039-4f51-11e8-80e2-005056bb6355' },
    { label: 'Все продукты', value: 'ef86e134-bdad-11e5-ad2a-0050569e61e6' }
]

const vatRateOptions = [
    { label: '0', value: '0' },
    { label: '12', value: '12' },
    { label: 'Без НДС', value: '322' }
]

const yearsOptions = [
    { label: '2019 г.', value: '2019' },
    { label: '2020 г.', value: '2020' },
    { label: '2021 г.', value: '2021' },
    { label: '2022 г.', value: '2022' },
    { label: '2023 г.', value: '2023' },
    { label: '2024 г.', value: '2024' },
    { label: '2025 г.', value: '2025' }
]

const monthsOptions = [
    { label: 'Январь', value: '01' },
    { label: 'Февраль', value: '02' },
    { label: 'Март', value: '03' },
    { label: 'Апрель', value: '04' },
    { label: 'Май', value: '05' },
    { label: 'Июнь', value: '06' },
    { label: 'Июль', value: '07' },
    { label: 'Август', value: '08' },
    { label: 'Сентябрь', value: '09' },
    { label: 'Октябрь', value: '10' },
    { label: 'Ноябрь', value: '11' },
    { label: 'Декабрь', value: '12' }
]

export default DataBlock3;