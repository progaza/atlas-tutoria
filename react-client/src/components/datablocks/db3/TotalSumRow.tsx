import React from 'react';

const TotalSumRow = ({ totalSum, totalSumInKzt }: { totalSum: number, totalSumInKzt: number }) => {
    return (
        <div style={{
            borderTop: 'solid 3px #0052cc',
            display: 'flex',
            flexDirection: 'row',
            flexWrap: 'nowrap',
            justifyContent: 'center',
            padding: '5px'
        }}>
            <span style={{ margin: '5px', padding: '10px', textDecoration: 'underline' }}>{'Итоговая сумма:'}</span>
            <span style={{ margin: '5px', padding: '10px', background: '#f4f5f7' }}>{(totalSum) ? totalSum : 0}</span>
            <span style={{ margin: '5px', padding: '10px', textDecoration: 'underline' }}>{'Итоговая сумма в KZT:'}</span>
            <span style={{ margin: '5px', padding: '10px', background: '#f4f5f7' }}>{(totalSumInKzt) ? totalSumInKzt : 0}</span>
        </div>
    );
}

export default TotalSumRow;