import React, { Component } from 'react';
import PoSelectList from '../../common/PoSelectList';
import PoTextArea from '../../common/PoTextArea';
import Attachment from '../../attachment/Attachment';
import Form, { FormHeader, FormSection, FormFooter } from '@atlaskit/form';
import ProjectConstants from '../../../core/constants/ProjectConstants'
import ErrorBoundary from '../../common/ErrorBoundary';
import Db4stateInterface from '../../../models/Db4stateInterface';
import MyOptionInterface from '../../../models/MyOptionInterface';
import { getIssueAction } from '../../../core/utils/jira.utils';
import { getApiCall } from '../../../core/api/api';

const { urls, issueActionTypes } = ProjectConstants

interface IProps {
  passState: any;
  curState?: Db4stateInterface;
  files?: File[];
  attachmentUploadProgress: number;
  onSelectFiles(files: File[]): void;
};


class DataBlock4 extends Component<IProps, Db4stateInterface> {
  constructor(props) {
    super(props)

    this.state = {
      datablock: 4,
      budgetYear: { label: '', value: '' },
      providerContacts: '',
      description: '',
      region: {
        available: [],
        isLoading: true,
        value: { label: '', value: '' }
      },
      isPrefilled: false,
      comment: '',
    }
  }

  render() {
    const { region, budgetYear, providerContacts, description, comment } = this.state;
    const { files, attachmentUploadProgress } = this.props;

    const issueAction = getIssueAction();
    const isActionUpdate = (issueAction === issueActionTypes.update);
    // console.log('render', 'files are', files);

    return (
      <Form onSubmit={data => console.log('form data', data)}>
        {({ formProps }) => (
          <form action='' id='db4' {...formProps}>
            <FormHeader title={"Блок № " + this.state.datablock} />
            <FormSection title="Регион, год и другие данные">
              <ErrorBoundary>
                <div className="field-group db4-content">
                  <PoSelectList
                    name='region'
                    label='Регион'
                    isRequired={false}
                    isLoading={region.isLoading}
                    options={region?.available}
                    placeholder='Выберите регион'
                    value={region?.value as MyOptionInterface}
                    onChange={value => this.onChangeHandler(value, 'region')}
                  />
                  <PoSelectList
                    name='budgetYear'
                    label='Бюджетный год'
                    isRequired={false}
                    options={budgetYearOptions}
                    placeholder='Выберите бюджетный год'
                    value={budgetYear as MyOptionInterface}
                    onChange={value => this.onChangeHandler(value, 'budgetYear')}
                  />
                  <PoTextArea
                    id="providerContacts"
                    name="providerContacts"
                    label='Контактные данные поставщика'
                    minimumRows={4}
                    spellCheck={true}
                    placeholder='Укажите контактные данные поставщика'
                    defaultValue={providerContacts}
                    onBlur={value => this.onChangeHandler(value, 'providerContacts')}
                  />
                  <PoTextArea
                    id="description"
                    name="description"
                    label='Описание'
                    minimumRows={4}
                    spellCheck={true}
                    placeholder='Укажите описание'
                    defaultValue={description}
                    isRequired={true}
                    onBlur={value => this.onChangeHandler(value, 'description')}
                  />
                  <Attachment
                    files={files}
                    // attachmentUploadProgress={uploadProgress}
                    attachmentUploadProgress={attachmentUploadProgress}
                    onSelectFiles={this.props.onSelectFiles}
                  />
                </div>
                <br />
                {(isActionUpdate) &&
                  <div>
                    <PoTextArea
                      id="comment"
                      name="comment"
                      label='Комментарий к доработке'
                      minimumRows={4}
                      spellCheck={true}
                      placeholder='Укажите комментарий к доработке'
                      defaultValue={comment}
                      isRequired={true}
                      onBlur={value => this.onChangeHandler(value, 'comment')}
                    />
                  </div>
                }
              </ErrorBoundary>
            </FormSection>
            <FormFooter>
            </FormFooter>
          </form>
        )}
      </Form>
    );
  }

  componentDidMount() {
    const areOptionsInitialized = (this.props.curState?.region?.available?.length)

    if (!areOptionsInitialized) {
      this.initAvailableOptions();
    }

    const curState = this.props.curState;
    // PRE-FILL for update or copy action
    const issueAction = getIssueAction();
    if (!curState) {
      console.log('curState is empty, so prefill is skipped, curState is ', curState);
      return;
    };
    const budgetYear2Find = ((curState.budgetYear as MyOptionInterface)?.value) ? (curState.budgetYear as MyOptionInterface).value : curState.budgetYear
    const region2Find = ((curState.region.value as MyOptionInterface)?.value) ? (curState.region.value as MyOptionInterface).value : curState.region.value

    const budgetYearOption = budgetYearOptions.find(item => item.value === budgetYear2Find)
    const regionOption = curState.region.available.find(item => item.value === region2Find)

    const { isPrefilled } = curState;
    if (issueAction === issueActionTypes.update) {
      console.log('case of UPDATE, set state from curState with description, curState is', curState);
      this.setState({
        budgetYear: budgetYearOption as MyOptionInterface,
        providerContacts: curState.providerContacts,
        description: curState.description,
        comment: curState?.comment,
        region: {
          available: curState.region.available,
          isLoading: curState.region.isLoading,
          value: regionOption as MyOptionInterface,
        },
        isPrefilled: true
      });
    } else if (issueAction === issueActionTypes.copy) {
      console.log('case of COPY, set state from curState without description, curState is', curState, '(isPrefilled) is', (isPrefilled));
      this.setState({
        budgetYear: budgetYearOption as MyOptionInterface,
        providerContacts: curState.providerContacts,
        description: (isPrefilled) ? curState.description : '',
        comment: curState?.comment,
        region: {
          available: curState.region.available,
          isLoading: curState.region.isLoading,
          value: regionOption as MyOptionInterface,
        },
        isPrefilled: true
      });
    }

    // PRE-FILL for transition between datablocks
    if (!(issueAction === issueActionTypes.copy || issueAction === issueActionTypes.update)) {
      console.log('Another case, set state from curState with description, curState is', curState);

      this.setState({
        budgetYear: budgetYearOption as MyOptionInterface,
        providerContacts: curState.providerContacts,
        description: curState.description,
        comment: curState?.comment,
        region: {
          available: curState.region.available,
          isLoading: curState.region.isLoading,
          value: regionOption as MyOptionInterface,
        },
        isPrefilled: true
      });
    }
  }

  initAvailableOptions() {
    // init regionOptions
    getApiCall(urls.getDataFrom1C + '?q=regions')
      .then(data => {
        if (!data) throw Error('data is empty')
        let updatedRegion = this.state.region;

        updatedRegion.available = data;
        updatedRegion.isLoading = false;

        this.setState({
          region: updatedRegion
        });
        return data;
      })
      .catch((ex) => { console.error('Error fetch/catch', ex); });
  }

  onChangeHandler(data, type) {
    switch (type) {
      case 'region':
        let updatedRegion = this.state.region;
        updatedRegion.value = data;
        this.setState({ region: updatedRegion });
        break;
      case 'budgetYear':
        this.setState({ budgetYear: data });
        break;
      case 'providerContacts':
        this.setState({ providerContacts: data.target.value });
        break;
      case 'description':
        this.setState({ description: data.target.value });
        break;
      case 'comment':
        this.setState({ comment: data.target.value });
        break;
      default:
        console.log('default', data, type)
        break;
    }

    setTimeout(() => { this.props.passState(this.state) }, 0);
  }

  // setFiles(files) {
  //   this.setState({
  //     files: files
  //   });
  // }
}

const budgetYearOptions = [
  { label: '2019 г.', value: '2019' },
  { label: '2020 г.', value: '2020' },
  { label: '2021 г.', value: '2021' },
  { label: '2022 г.', value: '2022' },
  { label: '2023 г.', value: '2023' },
  { label: '2024 г.', value: '2024' },
  { label: '2025 г.', value: '2025' }
];

export default DataBlock4;