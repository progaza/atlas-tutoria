import React, { Component, SyntheticEvent } from 'react';

import { Radio } from '@atlaskit/radio';
import ProviderItem from '../../models/ProviderItem';
import LoadingScreen from '../common/LoadingScreen';

const rowsBorderBottomValue = '1px solid #ddd'
const rowMarginValue = '5px'

interface Props {
    items: Array<ProviderItem>;
    setChecked: Function;
    isLoading: boolean;
}

interface State {
    isActive: boolean;
    isChecked: boolean;
    isFocused: boolean;
    isMouseDown: boolean;
    value: string;
}

export default class SearchResults extends Component<Props, State> {
    state = {
        value: '1',
        isActive: false,
        isChecked: false,
        isMouseDown: false,
        isFocused: false,
    };

    onBlur = () => {
        this.setState({
            isActive: this.state.isMouseDown && this.state.isActive,
            isFocused: false,
        });
    };

    onFocus = () => {
        this.setState({
            isFocused: true,
        });
    };

    onChange = ({ currentTarget: { value } }: SyntheticEvent<any>) => {
        this.props.setChecked(value);
    };

    render() {
        const { items, isLoading } = this.props;

        return (
            (isLoading)
                ? <LoadingScreen />
                : <div>
                    {(items?.length)
                        ? <table style={{ width: '100%' }}>
                            <thead>
                                <tr>
                                    <th style={{ width: 0 }} />
                                    <th>Наименование</th>
                                    <th>БИН</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    items.map(item => (
                                        <tr
                                            onClick={() => this.props.setChecked(item.guid)}
                                            key={`${item.guid}${item.name}${item.id}`}
                                        >
                                            <td style={{ width: 24, paddingRight: 0, borderBottom: rowsBorderBottomValue, margin: rowMarginValue }}>
                                                <Radio
                                                    isChecked={item.isChecked}
                                                    onBlur={this.onBlur}
                                                    onFocus={this.onFocus}
                                                    onChange={this.onChange}
                                                    name={item.name}
                                                    value={item.guid}
                                                />
                                            </td>
                                            <td style={{ borderBottom: rowsBorderBottomValue, margin: rowMarginValue }}>{item.name}</td>
                                            <td style={{ borderBottom: rowsBorderBottomValue, margin: rowMarginValue }}>{item.id}</td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </table>
                        : <div>Ничего не найдено</div>
                    }
                </div>
        );
    }
}