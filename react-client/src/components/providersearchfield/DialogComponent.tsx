import React, { Component } from 'react';
import Form, { FormSection, FormFooter } from '@atlaskit/form';
import ProjectConstants from '../../core/constants/ProjectConstants'
import ErrorBoundary from '../common/ErrorBoundary';
import PoTextField from '../common/PoTextField';
import PoButton from '../common/PoButton';
import { RadioGroup } from '@atlaskit/radio';
import { OptionsPropType } from '@atlaskit/radio/types';
import SearchResults from './SearchResults';
import { getApiCall } from '../../core/api/api';
import ProviderItem from '../../models/ProviderItem';

const { urls } = ProjectConstants

interface IState {
    items: ProviderItem[],
    providerInputText: string;
    searchByName: boolean;
    areProvidersLoading: boolean;
}
interface IProps {
    passChecked: Function;
}

class DialogComponent extends Component<IProps, IState> {
    constructor(props) {
        super(props)

        this.state = {
            items: [],
            providerInputText: '',
            searchByName: true,
            areProvidersLoading: false
        }
    }

    render() {
        return (
            <Form onSubmit={data => console.log('form data', data)}>
                {({ formProps }) => (
                    <form action='' id='dialogComponentForm' {...formProps}>
                        <FormSection>
                            <ErrorBoundary>
                                <span>Выберите тип поиска:</span>
                                <RadioGroup
                                    options={radioOptions}
                                    defaultValue={radioOptions[0].value}
                                    onChange={this.changeSearchTypeHandler}
                                />
                                <div style={{
                                    display: 'grid',
                                    gridTemplateColumns: '9fr 1fr',
                                    gridColumnGap: '5px',
                                    margin: '10px'
                                }}>
                                    <PoTextField
                                        id='providerInputText'
                                        name='providerInputText'
                                        label='Наименование поставщика'
                                        placeholder='Введите текст для поиска'
                                        defaultValue={''}
                                        onChange={value => { this.setState({ providerInputText: value.target.value }) }}
                                    />
                                    <PoButton style={{ alignSelf: 'end' }} appearance='primary' id='searchButtonId' inputText='Поиск' onSubmitClickHandler={this.searchProviderHandler} secondsToSpin={1} />
                                </div>
                            </ErrorBoundary>
                        </FormSection>
                        <br />
                        <hr />
                        <FormSection title="Результаты поиска">
                            <ErrorBoundary>
                                <div style={{ minHeight: '300px' }}><SearchResults items={this.state.items} setChecked={this.resultsSetChecked} isLoading={this.state.areProvidersLoading} /></div>
                            </ErrorBoundary>
                        </FormSection>
                        <FormFooter>
                        </FormFooter>
                    </form>
                )}
            </Form>
        )
    }

    searchProviderHandler = () => {
        const { searchByName, providerInputText } = this.state;
        let url = urls.searchProvider + '?id=' + providerInputText + '&amount=10';

        if (searchByName) {
            url = urls.searchProvider + '?name=' + providerInputText + '&amount=10';
        }

        this.setState({
            areProvidersLoading: true
        });

        getApiCall(url)
            .then(data => {
                this.setState({
                    items: data,
                    areProvidersLoading: false
                });
            })
            .catch(error => {
                console.error('searchProviderHandler, getApiCall, error', error);
                this.setState({
                    items: [],
                    areProvidersLoading: false
                });
            });

        // this.setState({
        //     items: [
        //         {
        //             id: '050540000690',
        //             guid: 'asd123',
        //             name: 'КУЗЕТ СЕРВИС КОКШЕТАУ ТОО',
        //             isChecked: true,
        //         },
        //         {
        //             id: '040340009209',
        //             guid: 'qqqq2222',
        //             name: '8&8 ТОО',
        //         },
        //     ]
        // })
    }

    changeSearchTypeHandler = (value) => {
        value = value.target.value;
        // console.log('changeSearchTypeHandler, value', value);
        if (value === 'id') {
            this.setState({
                searchByName: false
            });
        } else {
            this.setState({
                searchByName: true
            });
        }
    }

    resultsSetChecked = (value: string) => {
        // const newItems = this.state.items.slice().map(item => {
        const newItems = this.state.items.slice().map(item => {
            if (item.guid === value) {
                return {
                    ...item,
                    isChecked: true,
                };
            }
            return {
                ...item,
                isChecked: false,
            };
        });

        this.setState({
            items: newItems,
        });

        this.props.passChecked(newItems.find(item => item.isChecked));
    };
}

const radioOptions: OptionsPropType = [
    { name: 'searchType', value: 'name', label: 'по наименованию' },
    { name: 'searchType', value: 'id', label: 'по БИН' },
];

export default DialogComponent;