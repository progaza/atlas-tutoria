import React, { Component } from 'react';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import DialogComponent from './DialogComponent';
import ProviderItem from '../../models/ProviderItem';

interface IState {
    chosenProvider: ProviderItem;
    isOpen: boolean;
}
interface IProps {
    isOpen: boolean;
    closeDialog: any;
    setProvider: any;
}

class ModalDialog extends Component<IProps, IState> {
    constructor(props) {
        super(props)

        this.state = { isOpen: true, chosenProvider: { id: '', guid: '', name: '' } }
    }
    render() {
        const { isOpen, closeDialog, setProvider } = this.props;
        const { chosenProvider } = this.state;

        return (
            <ModalTransition>
                {isOpen && (
                    <Modal
                        actions={[
                            { text: 'OK', onClick: () => { setProvider({ label: chosenProvider.name, value: chosenProvider.id }) } },
                            { text: 'Cancel', onClick: closeDialog },
                        ]}
                        onClose={closeDialog}
                        heading="Поиск поставщика"
                    >
                        <DialogComponent passChecked={this.getCheckedProvider} />
                    </Modal>
                )}
            </ModalTransition>
        );
    }

    getCheckedProvider = (provider: ProviderItem) => {
        this.setState({
            chosenProvider: provider
        });
    }
}

export default ModalDialog;