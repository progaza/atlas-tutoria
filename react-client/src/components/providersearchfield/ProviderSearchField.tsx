import React, { Component, Fragment } from 'react';
import SearchIcon from '@atlaskit/icon/glyph/search';
import { Button } from '@atlaskit/button/dist/cjs/components/Button';
import Textfield from '@atlaskit/textfield';
import { Field } from '@atlaskit/form';
import ModalDialog from './ModalDialog';
import MyOptionInterface from '../../models/MyOptionInterface';

interface IProps {
    passProvider: Function;
    curState: MyOptionInterface | undefined;
    isDisabled: boolean;
}

interface IState {
    provider: MyOptionInterface,
    isDialogOpen?: boolean;
    isPrefilled: boolean;
}

class ProviderSearchField extends Component<IProps, IState> {
    constructor(props) {
        super(props);

        this.state = {
            provider: {
                label: '',
                value: ''
            },
            isDialogOpen: false,
            isPrefilled: false
        }
    }

    render() {
        const { provider, isDialogOpen } = this.state;

        const isCurStateEmpty = !Boolean(this.props.curState?.value);
        const isPrefilled = this.state.isPrefilled;
        if (!isCurStateEmpty && !isPrefilled) {
            this.setState({
                provider: {
                    label: this.props.curState?.label as string,
                    value: this.props.curState?.value as string
                },
                isPrefilled: true
            })
        }

        return (
            <div>
                <Field
                    label="Поставщик"
                    isRequired
                    name="provider">
                    {({ fieldProps, error, meta: { valid } }: any) => (
                        <Fragment>
                            <div style={{
                                display: 'grid',
                                gridTemplateColumns: '9fr 1fr',
                                gridColumnGap: '5px',
                            }}>
                                <Textfield value={provider.label} />
                                < Button
                                    iconAfter={
                                        <SearchIcon
                                            label='searchIcon'
                                        />
                                    }
                                    appearance="primary"
                                    style={{ alignSelf: 'center' }}
                                    isDisabled={this.props.isDisabled}
                                    onClick={() => { this.setState({ isDialogOpen: !isDialogOpen }) }}
                                >
                                </Button>
                            </div>
                        </Fragment>
                    )}
                </Field>

                <ModalDialog isOpen={isDialogOpen as boolean}
                    closeDialog={() => { this.setState({ isDialogOpen: !isDialogOpen }) }}
                    setProvider={(provider: MyOptionInterface) => {
                        this.setState({ isDialogOpen: !isDialogOpen, provider: provider });
                        this.props.passProvider(provider);
                    }} />
            </div>
        );
    }
}

export default ProviderSearchField;