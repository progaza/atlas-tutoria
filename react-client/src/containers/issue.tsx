import React, {Component, Fragment} from 'react';
import AppStateInterface from "../models/AppStateInterface";
import Textfield from "@atlaskit/textfield";
import Tabs from '@atlaskit/tabs';
import {CreateIssue} from "../components/tab/create-issue";
import MyOptionInterface from "../models/MyOptionInterface";

const receiverOptions: Array<MyOptionInterface> = [
    {
        label: 'Fakhrudinov Azamat',
        value: 'id1234'
    },
    {
        label: 'Jumash Miras',
        value: 'id9929'
    },
    {
        label: 'Sam Wilson',
        value: 'id0234'
    }
];

const tabs = [
    {
        label: 'Tab 1',
        content:
            <div style={{width: '100%', paddingInline: '20%'}}>
                <CreateIssue
                    // @ts-ignore
                    receiverOptions={receiverOptions}></CreateIssue>
            </div>
    },
    {label: 'Tab 2', content: <div>Two</div>},
    {label: 'Tab 3', content: <div>Three</div>},
];

export class Issue extends Component<{}, AppStateInterface> {
    constructor(props) {
        super(props);
        this.state = {
            // @ts-ignore
            greeting: {
                name: '',
                content: ''
            },
            hella: ''
        };

    }

    saveGreeting() {
        // @ts-ignore
        console.log('GREETING: ', this.state.greeting);
    }

    render() {
        return (
            <div>
                <Tabs tabs={tabs}></Tabs>
                <h1>Issue Crud plugin</h1>
                <Textfield name="basic" aria-label="default text field"/>
                <button        // @ts-ignore
                    onClick={() => this.saveGreeting()}>Save greeting
                </button>
            </div>
        );
    }
}

const styles = {
    tabStyle: {
        width: '100%',
        marginInline: '20%'
    } as React.CSSProperties
}