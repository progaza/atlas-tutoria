import React, { Component } from 'react';
import axios from 'axios';

import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import { Grid, GridColumn } from '@atlaskit/page';

import PageSwitcherMenu from '../components/pageswitchermenu/PageSwitcherMenu';
import CreateIssueForm from '../components/createissueform/CreateIssueForm';
import ProjectConstants, { RequiredFieldModel, RequiredFieldsData, environment, ENVIRONMENT_PRODUCTION } from '../core/constants/ProjectConstants';
import PermissionErrorScreen from '../components/common/PermissionErrorScreen';
import LoadingScreen from '../components/common/LoadingScreen';
import SubmitLoadingScreen from '../components/common/SubmitLoadingScreen';
import { AttachmentService } from '../core/services/AttachmentService';
import { getApiCall, postApiCall, putApiCall } from '../core/api/api';
import { MyStoreInterface } from '../models/MyStoreInterface';

import './App.css'
import { getAjsMeta, getIssueAction, getIssueKey, getSumFromTableData } from '../core/utils/jira.utils';
import { validationCheckBlock1, validationCheckBlock3, validationCheckBlock4 } from './ValidationChecks';
import MyOptionInterface from '../models/MyOptionInterface';
import AppStateInterface from '../models/AppStateInterface';
import { documentTypeOptions } from '../components/datablocks/db2/MyTableDataDocumentType';
import { Db2MyTableStateInterface } from '../models/Db2stateInterface';
import BannedBrowserErrorScreen from '../components/common/BannedBrowserErrorScreen';

let myStore: MyStoreInterface;


const { urls, projectKey, issuetypeId, fields, issueActionTypes } = ProjectConstants

export class App extends Component<{}, AppStateInterface> {
  constructor(props) {
    super(props);

    this.handlePickOfTheForm = this.handlePickOfTheForm.bind(this);
    this.onSubmitClickHandler = this.onSubmitClickHandler.bind(this);
    this.passState = this.passState.bind(this);
    this.hadleErrorDialogClosing = this.hadleErrorDialogClosing.bind(this);
    this.setFiles = this.setFiles.bind(this);
    this.checkReporter = this.checkReporter.bind(this);

    this.state = { activeBlock: 'datablock1', childrensState: '', _showErrorDialog: false, _invalidFields: [], attachmentUploadProgress: 0, filesUploaded: false, isLoading: true, showErrorScreen: false, isSubmitted: false, issue: { id: '', key: '', guid: '' }, systemTextStorage0: '', isBannedBrowserUsed: false };
  }

  render() {
    const activeBlock = this.state.activeBlock;
    const activeBlockLength = activeBlock.length;
    const activeBlockId = Number(activeBlock.substr(activeBlockLength - 1, activeBlockLength));

    const errDialogActions = [
      { text: 'Ok', onClick: this.hadleErrorDialogClosing },
    ];

    const invalidFieldsMsgs: Array<RequiredFieldModel> = this.state?._invalidFields;
    const { files, attachmentUploadProgress, isSubmitted, isLoading, showErrorScreen, isBannedBrowserUsed } = this.state;

    let component2Show;
    if (isBannedBrowserUsed) {
      component2Show = <BannedBrowserErrorScreen />
    } else if (showErrorScreen) {
      component2Show = <PermissionErrorScreen />
    } else if (isSubmitted) {
      component2Show = <SubmitLoadingScreen />
    } else if (isLoading) {
      component2Show = <LoadingScreen />
    }
    else {
      component2Show =
        <div>
          <Grid spacing="cosy">
            <GridColumn medium={2}>
              <PageSwitcherMenu selectedButton={activeBlock} onPickOfTheForm={this.handlePickOfTheForm} onSubmitClickHandler={this.onSubmitClickHandler} />
            </GridColumn>
            <GridColumn medium={10}>
              <CreateIssueForm selectedBlock={activeBlockId} passState={this.passState} files={files} attachmentUploadProgress={attachmentUploadProgress} onSelectFiles={this.setFiles} />
            </GridColumn>
          </Grid>
          <div>
            <ModalTransition>
              {this.state._showErrorDialog && (
                <Modal actions={errDialogActions} onClose={this.hadleErrorDialogClosing} heading="Внимание!" appearance='warning' autoFocus={true} >
                  {'Просьба обратите внимание на следующее:'} <br />
                  <ul>
                    {(invalidFieldsMsgs.length) ? invalidFieldsMsgs.map((it) => <li><Row key={it.title} message={it.errorMessage} /></li>) : ''}
                  </ul>
                  <br />
                  {'Спасибо!'}
                </Modal>
              )}
            </ModalTransition>
          </div>
        </div>
    }

    return component2Show;
  }

  componentDidMount() {
    // get issue key from path
    const issueKey: string = getIssueKey();
    console.log('issueKey', issueKey);

    let isLoadingVal = true;
    if (issueKey !== '') {
      this.checkReporter(issueKey);
    } else {
      isLoadingVal = false;
    }

    const isBannedBrowserUsed = checkIsUsedBrowserBanned();
    if (isBannedBrowserUsed) {
      this.setState({
        isLoading: isLoadingVal,
        isBannedBrowserUsed: true
      });
    } else {
      this.setState({
        isLoading: isLoadingVal,
        isBannedBrowserUsed: false
      });
    }

    const issueAction = getIssueAction();
    if (issueAction) {
      this.setState({
        issueAction: issueAction
      });
    }
  }

  handlePickOfTheForm(selectedBlock) {
    // set validation 
    // check validity
    var invalidFields: Array<any> = [];

    const isToUpdate: boolean = Boolean(this.state.issueAction === issueActionTypes.update);
    if (this.state.activeBlock === 'datablock1') {
      invalidFields = validationCheckBlock1(myStore);
    } else if (this.state.activeBlock === 'datablock3') {
      invalidFields = validationCheckBlock3(myStore);
    } else if (this.state.activeBlock === 'datablock4') {
      invalidFields = validationCheckBlock4(myStore, isToUpdate);
    }

    if (!invalidFields.length) {
      this.setState({ activeBlock: selectedBlock });
    } else {
      // show error dialog
      this.setState({
        _showErrorDialog: true,
        _invalidFields: invalidFields
      });
    }
  }

  hadleErrorDialogClosing() {
    this.setState({
      _showErrorDialog: false,
      sumWasIncreased: false,
    });
  }

  onSubmitClickHandler() {
    // сначала отправить запрос на создание заявки JIRA, получить issueId ->
    // далее этот issueId уже понадобится для создания завяки ФК

    // check all validations if everything is valid then start submitting process, else show errorDialog
    const isToUpdate: boolean = this.state.issueAction === issueActionTypes.update
    var invalidFields: Array<any> = [];
    const invalidFieldsBlock1 = validationCheckBlock1(myStore)
    if (invalidFieldsBlock1 && invalidFieldsBlock1.length > 0) invalidFields.push(...invalidFieldsBlock1);

    const invalidFieldsBlock3 = validationCheckBlock3(myStore)
    if (invalidFieldsBlock3 && invalidFieldsBlock3.length > 0) invalidFields.push(...invalidFieldsBlock3);

    const invalidFieldsBlock4 = validationCheckBlock4(myStore, isToUpdate)
    if (invalidFieldsBlock4 && invalidFieldsBlock4.length > 0) invalidFields.push(...invalidFieldsBlock4);

    console.log('onSubmitClickHandler, isToUpdate is ', isToUpdate);
    if (isToUpdate) {
      // check 
      const tableDataValue = myStore.db3state.tableData
      const sumFromTableData = getSumFromTableData(tableDataValue);

      const sumWasIncreased = sumFromTableData > Number(myStore.validationData?.initialSum)

      // if true, then stop submit process and show error form
      if (sumWasIncreased) {
        invalidFields.push(RequiredFieldsData.edit.sumIncreasedError);
      }
    }

    if (!invalidFields.length) {
      this.setState({
        isSubmitted: true
      });

      // send create request to create issue in JIRA
      this.sendCreateIssueRequest();
    } else {
      // show error dialog
      this.setState({
        _showErrorDialog: true,
        _invalidFields: invalidFields
      });
    }
  }

  sendCreateIssueRequest() {
    let requestBody = this.getRequestBody();

    // check if need to update instead of create
    const issue = this.state.issue
    const isToUpdate: boolean = (this.state.issueAction === issueActionTypes.update);
    console.log('sendCreateIssueRequest, isToUpdate is ', isToUpdate);
    if (isToUpdate) {
      // if true then, add additional parameters to request body
      // and call put request
      let updatedRequestBodyObject = JSON.parse(requestBody);

      if (environment === ENVIRONMENT_PRODUCTION) {
        Object.assign(updatedRequestBodyObject.fields, { customfield_27907: issue?.guid });
      } else {
        Object.assign(updatedRequestBodyObject.fields, { customfield_23800: issue?.guid });
      }

      // special checks for editing:
      // check if at least one budget item was changed, if so, then add flag budgetItemWasChanged:true

      const systemTextStorage0 = this.state.systemTextStorage0;
      let updatedSystemTextStorage0
      if (systemTextStorage0) {
        updatedSystemTextStorage0 = JSON.parse(systemTextStorage0)
        updatedSystemTextStorage0.budgetItemWasChanged = myStore.db3state.budgetItemWasChanged;
      } else {
        updatedSystemTextStorage0 = {
          budgetItemWasChanged: false,
        }
      }

      if (environment === ENVIRONMENT_PRODUCTION) {
        Object.assign(updatedRequestBodyObject.fields, { customfield_25401: JSON.stringify(updatedSystemTextStorage0) });
      } else {
        Object.assign(updatedRequestBodyObject.fields, { customfield_25017: JSON.stringify(updatedSystemTextStorage0) });
      }

      putApiCall(urls.updateIssue + '?issue=' + issue?.id, JSON.stringify(updatedRequestBodyObject))
        .then(response => {
          // send create request to create application in 1C
          this.sendCreateApplicationRequestInExternalSystem(issue?.id);
        })
        .catch((ex) => {
          console.error('Error fetch/catch', ex);
        });

      postApiCall(urls.createIssue + '/' + issue?.id + '/comment', JSON.stringify({ body: `При редактировании заявки автором, был добавлен следующий комментарий: "${myStore.db4state.comment}"` }));

    } else {
      // else, then call post request
      postApiCall(urls.createIssue, requestBody)
        .then(data => {
          const jiraIssueId = data?.id;
          // send create request to create application in 1C
          this.sendCreateApplicationRequestInExternalSystem(jiraIssueId);
        })
        .catch((ex) => { console.error('Error fetch/catch', ex); });
    }
  }

  async sendCreateApplicationRequestInExternalSystem(jiraIssueId) {
    let requestBody = await this.getRequestBody4ExternalSystem(jiraIssueId, myStore);

    postApiCall(urls.postCreatePaymentOrderTicket, requestBody)
      .then(async data => {
        // if response from external system is succes, then
        const isResponseSuccess = (data?.returnProp === 'success')

        if (isResponseSuccess) {
          // send request to add comment to issue
          // this.sendAddCommentRequest(jiraIssueId, data);
          this.sendAddCommentRequest(jiraIssueId, 'Данные в 1С были успешно отправлены');

          // send request to update flags and approvers
          // this.sendUpdateIssueRequest(jiraIssueId, data);
        }

        // handle upload of files
        try {
          this.setState({ attachmentUploadProgress: 0 });

          const { files, filesUploaded } = this.state;
          const fid = jiraIssueId; // TODO find out what for this data and what is it
          if (!filesUploaded && files) {
            const cancelToken = axios.CancelToken.source().token;
            await AttachmentService.doUpload(
              { fid, files },
              (progress) => { this.setState({ attachmentUploadProgress: progress }) },
              cancelToken
            );

            this.setState({
              filesUploaded: true
            });
          }

        } catch (error) {
          console.error(error);
        }

        // redirect to issue page
        this.getIssueKeyAndRedirect(jiraIssueId);

      })
      .catch((ex) => { console.error('Error fetch/catch', ex); });
  }

  sendAddCommentRequest(jiraIssueId, responseData) {
    const responseAsString = JSON.stringify(responseData);
    const requestBody = {
      'body': responseAsString
    };

    postApiCall(urls.createIssue + '/' + jiraIssueId + '/comment', JSON.stringify(requestBody));
  }

  getIssueKeyAndRedirect(issueId) {
    let issueKey;
    const url = urls.createIssue + '/' + issueId;

    getApiCall(url)
      .then(data => {
        issueKey = data?.key

        // redirect 
        const issueUrl = urls.issueBaseUrl + '/' + issueKey;


        // // wait while issueStatusCode is not changed from Open, then redirect
        // // but if we tried 3 times, then redirect too
        // let tryCounter = 0;
        // let isStatusStillOpen = true;
        // const OPEN_STATUS_ID = 1;
        // let currentIssueStatusID = 0;

        // while (tryCounter < 3 && isStatusStillOpen) {
        //   // check statusId
        //   getApiCall(urls.createIssue + issueKey)
        //     .then(data => {
        //       currentIssueStatusID = Number(data.fields.status.id);
        //       console.log('currentIssueStatusID is ', currentIssueStatusID);

        //       if(currentIssueStatusID !== OPEN_STATUS_ID) isStatusStillOpen = false;
        //       tryCounter++;
        //     })
        // }

        redirectFunc(issueUrl);
      })
      .catch((ex) => { console.error('Error fetch/catch', ex); });
  }

  getRequestBody() {
    const db1state = myStore?.db1state;
    const db4state = myStore?.db4state;
    const provider = {
      'label': db1state?.provider?.chosen?.chosenProvider?.label,
      'value': db1state?.provider?.chosen?.chosenProvider?.value
    };
    const chosenContract = db1state?.chosenContract;

    const contract = chosenContract?.label;
    const description = myStore?.db4state?.description;
    const providerContacts = myStore?.db4state?.providerContacts;
    const priority = (db1state?.priority as MyOptionInterface)?.value;

    let customFieldsObject = new Map();
    let cfValueForStorage = new Map();

    const cfProvider = fields.provider;
    const cfProviderId = fields.providerId;
    const cfContractNum = fields.contractNumber;
    const cfContractSum = fields.contractSum;
    const cfCurrency = fields.currency;
    const cfBudgetYear = fields.budgetYear;
    const cfContactData = fields.contactData;
    const cfRegion = fields.region;
    const cfProcurementType = fields.procurementType;
    const cfTableData_1 = fields.tableData_1;
    const cfTableData_2 = fields.tableData_2;
    const cfTotalSum = fields.totalSum;

    const cfTableData_1Value = getValueForTableData(1, 'label');
    const cfTableData_2Value = getValueForTableData(2, 'label');

    const cfBudgetYearValue = ((db4state?.budgetYear as MyOptionInterface)?.value) ? { 'value': (db4state?.budgetYear as MyOptionInterface)?.value } : null;
    const cfRegionValue = (db4state?.region?.value as MyOptionInterface)?.label;
    const cfCurrencyValue = chosenContract?.currency;
    const cfContractSumValue = chosenContract?.sum;
    const cfProcurementTypeValue = (chosenContract?.procurementType) ? { 'value': chosenContract?.procurementType } : null;
    const application4SpendingOption = {
      label: db1state.application4Spending?.name,
      value: db1state.application4Spending?.value
    }
    const cfTotalSumValue = myStore.db3state.totalSum;

    customFieldsObject = this.checkAndAddToMap(customFieldsObject, cfProvider, provider.label);
    customFieldsObject = this.checkAndAddToMap(customFieldsObject, cfProviderId, provider.value);
    customFieldsObject = this.checkAndAddToMap(customFieldsObject, cfContractNum, contract);
    customFieldsObject = this.checkAndAddToMap(customFieldsObject, cfBudgetYear, cfBudgetYearValue);
    customFieldsObject = this.checkAndAddToMap(customFieldsObject, cfContactData, providerContacts);
    customFieldsObject = this.checkAndAddToMap(customFieldsObject, cfRegion, cfRegionValue);
    customFieldsObject = this.checkAndAddToMap(customFieldsObject, cfCurrency, cfCurrencyValue);
    customFieldsObject = this.checkAndAddToMap(customFieldsObject, cfContractSum, Number(cfContractSumValue));
    customFieldsObject = this.checkAndAddToMap(customFieldsObject, cfProcurementType, cfProcurementTypeValue);
    customFieldsObject = this.checkAndAddToMap(customFieldsObject, cfTableData_1, cfTableData_1Value); // table data1
    customFieldsObject = this.checkAndAddToMap(customFieldsObject, cfTableData_2, cfTableData_2Value); // table data2
    customFieldsObject = this.checkAndAddToMap(customFieldsObject, cfTotalSum, Number(cfTotalSumValue));

    const cfTableData_1ValuesForStorage = getValueForTableData(1, 'value');
    const cfTableData_2ValuesForStorage = getValueForTableData(2, 'value');
    cfValueForStorage = this.checkAndAddToMap(cfValueForStorage, cfProviderId, provider.value);
    cfValueForStorage = this.checkAndAddToMap(cfValueForStorage, cfContractNum, chosenContract?.value);
    cfValueForStorage = this.checkAndAddToMap(cfValueForStorage, cfBudgetYear, cfBudgetYearValue);
    cfValueForStorage = this.checkAndAddToMap(cfValueForStorage, cfContactData, providerContacts);
    cfValueForStorage = this.checkAndAddToMap(cfValueForStorage, cfRegion, (db4state?.region?.value as MyOptionInterface)?.value);
    cfValueForStorage = this.checkAndAddToMap(cfValueForStorage, cfCurrency, cfCurrencyValue);
    cfValueForStorage = this.checkAndAddToMap(cfValueForStorage, cfContractSum, Number(cfContractSumValue));
    cfValueForStorage = this.checkAndAddToMap(cfValueForStorage, cfProcurementType, cfProcurementTypeValue);
    cfValueForStorage = this.checkAndAddToMap(cfValueForStorage, cfTableData_1, cfTableData_1ValuesForStorage); // table data1
    cfValueForStorage = this.checkAndAddToMap(cfValueForStorage, cfTableData_2, cfTableData_2ValuesForStorage); // table data2
    cfValueForStorage = this.checkAndAddToMap(cfValueForStorage, 'application4Spending', JSON.stringify(application4SpendingOption));
    cfValueForStorage = this.checkAndAddToMap(cfValueForStorage, 'contractsOptions', JSON.stringify(db1state.contractsOptions));
    cfValueForStorage = this.checkAndAddToMap(cfValueForStorage, 'appl4spendingAvailableOptions', JSON.stringify(db1state.application4Spending?.availableOptions));
    cfValueForStorage = this.checkAndAddToMap(cfValueForStorage, 'regionAvailableOptions', JSON.stringify(db4state.region.available));
    cfValueForStorage = this.checkAndAddToMap(cfValueForStorage, cfTotalSum, String(cfTotalSumValue));

    // cfValueForStorage = this.checkAndAddToMap(cfValueForStorage, fields.systemTextStorage, JSON.stringify(Object.fromEntries(cfValueForStorage)));

    customFieldsObject = this.checkAndAddToMap(customFieldsObject, fields.systemTextStorage, JSON.stringify(Object.fromEntries(cfValueForStorage)));

    const mapAsObject = Object.fromEntries(customFieldsObject);

    const reqBody = (this.state.issueAction === issueActionTypes.update)
      ? JSON.stringify({
        "fields": {
          "summary": "Заявка на оплату - " + provider.label,
          "description": description,
          "priority": {
            "id": priority
          },
          ...mapAsObject,
        }
      })
      : JSON.stringify({
        "fields": {
          "project": {
            "key": projectKey
          },
          "issuetype": {
            "id": issuetypeId
          },
          "summary": "Заявка на оплату - " + provider.label,
          "description": description,
          "priority": {
            "id": priority
          },
          ...mapAsObject,
        }
      });

    return reqBody
  }

  checkAndAddToMap(map, cf, cfVal) {
    if (cfVal) {
      map.set(cf, cfVal);
    }

    return map
  }

  async getRequestBody4ExternalSystem(jiraIssueId, state: MyStoreInterface) {
    let mainRequestPart = await this.prepareMainRequestPart(jiraIssueId, state);
    const secondaryPart = this.prepareSecondaryPart(state);

    console.log('mainRequestPart', mainRequestPart);
    console.log('secondaryPart', secondaryPart);

    mainRequestPart.requestContent = secondaryPart

    const reqBody = JSON.stringify(mainRequestPart);

    return reqBody
  }

  async prepareMainRequestPart(jiraIssueId, state: MyStoreInterface) {
    const db1state = state?.db1state;
    const db4state = state?.db4state;

    const providerVal = db1state?.provider?.chosen?.chosenProvider?.value;
    const contractVal = db1state?.chosenContract?.value;
    const currency = db1state?.chosenContract?.currency;
    const amount = db1state?.chosenContract?.sum;

    // const availableDepartments = db3state?.options?.clients?.available;
    const department = db1state?.department;
    // const departmentGuid = availableDepartments.find(dep => {
    //   return dep.label.toLowerCase() === department.toLowerCase()
    // })?.value
    const departmentGuid = await getDepatmentGuidByName(department);

    const currentDateFormatted = getFormattedCurrentDate();
    const guid = db1state?.application4Spending?.value

    const reporter = db1state.username;

    return {
      guid: (guid) ? guid : '',
      requestID: jiraIssueId,
      requestDate: currentDateFormatted,
      reporter: reporter,
      currencyGuid: currency,
      contractGuid: contractVal,
      supplierGuid: providerVal,
      organizationGuid: '072be93e-1f2f-11e5-8ed2-0050569e790d',
      departmentGuid: departmentGuid.value,
      amount: (amount) ? amount : 0,
      itemsDescription: db4state?.description,
      requestContent: [{}]
    }
  }

  prepareSecondaryPart(state) {
    const rows = state?.db3state?.tableData;
    const result = rows.map(row => {
      return {
        clientGuid: row?.client?.value,
        itemGuid: row?.nomenclature?.value,
        itemCharacteristicsGuid: row?.nomenclatureCharacteristic?.value,
        measurementUnitGuid: row?.unitOfMeasurement?.value,
        budgetMonth: row?.reportedMonth?.value,
        budgetYear: row?.reportedYear?.value,
        amount: row?.sum,
        budgetArticleGuid: row?.budgetItem?.value,
        regionGuid: row?.organizationUnit?.value,
        NDSrate: row?.vatRate?.value,
        quantity: row?.amount,
        productType: row?.productType?.label,
        Project: ''
      }
    })

    return result
  }

  passState(data) {
    // this.setState({ childrensState: data });
    myStore = data;
  }

  setFiles(files) {
    this.setState({
      files: files
    });
  }

  async checkReporter(issueKey) {
    // get reporter of that issue, check if this user is not reporter
    let issueReporter: string = '';

    // get current user 
    const currentUser = getAjsMeta("remote-user");
    console.log('currentUser', currentUser);

    //check if form was already pre-filled
    getApiCall(urls.createIssue + '/' + issueKey)
      .then(data => {
        issueReporter = data.fields.reporter.name
        console.log('issueReporter is ', issueReporter, 'currentUser is ', currentUser);
        if (currentUser !== issueReporter) {
          // then show error screen
          this.setState({
            isLoading: false,
            showErrorScreen: true
          });
        } else {
          this.setState({
            isLoading: false,
            showErrorScreen: false,
            issue: {
              key: issueKey,
              id: data.id,
              guid: data.fields[fields.guid]
            },
            systemTextStorage0: data.fields[fields.systemTextStorage_0],
          });
        }
      });
  }
}

function getFormattedCurrentDate() {
  const d = new Date()
  const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d)
  const mo = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(d)
  const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d)

  return `${ye}-${mo}-${da}`
}

function redirectFunc(url) {
  setTimeout(() => window.location = url, 5000);
}

function Row(props) {
  return <p>{props.message}</p>;
}

function getValueForTableData(tableNumber: Number, neededValue: String = "label") {
  if (neededValue === "label") {
    if (tableNumber === 1) {
      const initialData: Db2MyTableStateInterface[] = myStore?.db2state?.tableData;

      if (isInitialDataEmpty(initialData)) {
        console.log('initialData is empty');
        return;
      }

      let convertedData = initialData.map(row => {
        if ((row.documentType as MyOptionInterface)?.label) {
          return {
            'document': {
              'type': (row.documentType as MyOptionInterface).label,
              'number': row.documentNum,
              'date': row.documentDate,
            }
          }
        } else {
          const documentType: MyOptionInterface | undefined = documentTypeOptions.find(options => options.value === row.documentType)
          console.log('documentType is', documentType);

          return {
            'document': {
              'type': documentType?.label,
              'number': row.documentNum,
              'date': row.documentDate,
            }
          }
        }
      });

      const result = {
        "fields": [
          {
            "label": "Тип документ",
            "name": "document.type"
          },
          {
            "label": "Номер документа",
            "name": "document.number"
          },
          {
            "label": "Дата документа",
            "name": "document.date"
          }
        ],
        "data": convertedData
      };

      return JSON.stringify(result);
    } else if (tableNumber === 2) {
      const initialData = myStore?.db3state?.tableData;
      if (!initialData.length) {
        console.log('initialData is empty');
        return;
      }

      const convertedData = initialData.map(row => {
        return {
          'row': {
            'idRow': row.idRow,
            'client': (row.client as MyOptionInterface).label,
            'organizationUnit': (row.organizationUnit as MyOptionInterface).label,
            'productType': (row.productType as MyOptionInterface)?.label,
            'nomenclature': (row.nomenclature as MyOptionInterface).label,
            'nomenclatureCharacteristic': (row?.nomenclatureCharacteristic as MyOptionInterface)?.label,
            'unitOfMeasurement': (row.unitOfMeasurement as MyOptionInterface).label,
            'reportedYear': (row.reportedYear as MyOptionInterface).label,
            'reportedMonth': (row.reportedMonth as MyOptionInterface).label,
            'vatRate': (row.vatRate as MyOptionInterface).label,
            'amount': row.amount,
            'sum': row.sum,
            'budgetItem': (row.budgetItem as MyOptionInterface).label
          }
        }
      });

      const result = {
        "fields": [
          {
            "label": "Клиент",
            "name": "row.client"
          },
          {
            "label": "Подразделение организации",
            "name": "row.organizationUnit"
          },
          {
            "label": "Тип продукта",
            "name": "row.productType"
          },
          {
            "label": "Наименование ТРУ",
            "name": "row.nomenclature"
          },
          {
            "label": "Характеристика ТРУ",
            "name": "row.nomenclatureCharacteristic"
          },
          {
            "label": "Единица измерения",
            "name": "row.unitOfMeasurement"
          },
          {
            "label": "Отчетный год",
            "name": "row.reportedYear"
          },
          {
            "label": "Отчетный месяц",
            "name": "row.reportedMonth"
          },
          {
            "label": "Ставка НДС",
            "name": "row.vatRate"
          },
          {
            "label": "Количество",
            "name": "row.amount"
          },
          {
            "label": "Сумма",
            "name": "row.sum"
          },
          {
            "label": "Статья бюджета",
            "name": "row.budgetItem"
          },
          {
            "label": "№ строки",
            "name": "row.idRow"
          }
        ],
        "data": convertedData
      };

      return JSON.stringify(result);
    } else {
      console.error('ERROR, getValueForTableData, no such tableNumber');
      return;
    }
  } else {
    if (tableNumber === 1) {
      const initialData: Db2MyTableStateInterface[] = myStore?.db2state?.tableData;

      if (isInitialDataEmpty(initialData)) {
        console.log('initialData is empty');
        return;
      }

      const convertedData = initialData.map(row => {
        if ((row.documentType as MyOptionInterface)?.value) {
          return {
            'document': {
              'type': (row.documentType as MyOptionInterface).value,
              'number': row.documentNum,
              'date': row.documentDate,
            }
          }
        } else {
          const documentType: MyOptionInterface | undefined = documentTypeOptions.find(options => options.value === row.documentType)

          return {
            'document': {
              'type': documentType?.value,
              'number': row.documentNum,
              'date': row.documentDate,
            }
          }
        }
      });

      const result = {
        "data": convertedData
      };

      return JSON.stringify(result);
    } else if (tableNumber === 2) {
      const initialData = myStore?.db3state?.tableData;
      if (!initialData.length) {
        console.log('initialData is empty');
        return;
      }

      const convertedData = initialData.map(row => {
        return {
          'row': {
            'idRow': row.idRow,
            'client': (row.client as MyOptionInterface).value,
            'organizationUnit': (row.organizationUnit as MyOptionInterface).value,
            'productType': (row?.productType as MyOptionInterface)?.value,
            'nomenclature': (row.nomenclature as MyOptionInterface).value,
            'nomenclatureCharacteristic': (row?.nomenclatureCharacteristic as MyOptionInterface)?.value,
            'unitOfMeasurement': (row.unitOfMeasurement as MyOptionInterface).value,
            'reportedYear': (row.reportedYear as MyOptionInterface).value,
            'reportedMonth': (row.reportedMonth as MyOptionInterface).value,
            'vatRate': (row.vatRate as MyOptionInterface).value,
            'amount': row.amount,
            'sum': row.sum,
            'budgetItem': (row.budgetItem as MyOptionInterface).value
          }
        }
      });

      const result = {
        "data": convertedData
      };

      return JSON.stringify(result);
    } else {
      console.error('ERROR, getValueForTableData, no such tableNumber');
      return;
    }
  }
}

async function getDepatmentGuidByName(departmentName) {
  const url = ProjectConstants.urls.getDataFrom1C + "?q=departmentGuidByName&v=" + departmentName
  const response = await fetch(url);
  const data = await response.json();

  if (data === null) return;

  return data[0];
}

const isInitialDataEmpty = (initialData: Db2MyTableStateInterface[]) => {
  if (initialData === undefined || initialData === null) return true;

  if (initialData.length < 1) return true;

  if (initialData.length === 1 && !Boolean(initialData[0].documentNum)) return true;

  return false;
}

const checkIsUsedBrowserBanned = (): boolean => {
  const userAgentData = navigator.userAgent;
  return (userAgentData.indexOf('MSIE ') > 0 || userAgentData.indexOf('Trident/') > 0)
}