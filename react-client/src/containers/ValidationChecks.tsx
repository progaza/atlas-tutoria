import { RequiredFieldsData, RequiredFieldModel, RequiredFieldTypes } from '../core/constants/ProjectConstants';
import { MyStoreInterface } from '../models/MyStoreInterface';
import { Db3MyTableStateInterface } from '../models/Db3stateInterface';
import MyOptionInterface from '../models/MyOptionInterface';

export function validationCheckBlock1(myStore: MyStoreInterface) {
    const dbData = myStore?.db1state;
    var invalidFields: Array<any> = [];

    // provider
    if (!dbData?.provider?.chosen?.chosenProvider?.value) invalidFields.push(RequiredFieldsData.db1.provider);

    // chosenContract
    if (!dbData?.chosenContract?.value) invalidFields.push(RequiredFieldsData.db1.contractNum);

    // application4Spending
    // const availableOptionsLength = dbData?.application4Spending?.availableOptions?.length
    // if ((availableOptionsLength && availableOptionsLength > 0) && !dbData?.application4Spending?.value) invalidFields.push(RequiredFieldsData.db1.applicationForSpending);

    // disabled because there is a default value
    // priority
    // if (!db1Data?.priority?.value) invalidFields.push(RequiredFieldsData.db1.priority);

    return invalidFields;
}

export function validationCheckBlock3(myStore: MyStoreInterface) {
    const dbData = myStore?.db3state;
    var invalidFields: Array<any> = [];

    let tableData: Db3MyTableStateInterface[] = [];
    if (dbData?.tableData?.length) {
        tableData = dbData?.tableData;
    } else {
        if (myStore?.tableData) {
            // data was prefilled, so check is not needed
            return [];
        }
    }

    // if data was prefilled  then this should pass, but it didn't
    if (!tableData?.length) return [new RequiredFieldModel('db-block3', RequiredFieldTypes.GENERAL, '', 'Заполните обязательные поля блока 3!')];

    tableData?.forEach(row => {
        const { idRow, client, organizationUnit, nomenclature, reportedYear, reportedMonth, sum, amount, vatRate, budgetItem } = row
        // client
        if (!(client as MyOptionInterface)?.value) invalidFields.push(new RequiredFieldModel('Клиент', RequiredFieldTypes.TABLE_ROW, idRow as string));

        // organizationUnit
        if (!(organizationUnit as MyOptionInterface)?.value) invalidFields.push(new RequiredFieldModel('Подразделение организации', RequiredFieldTypes.TABLE_ROW, idRow as string));

        // nomenclature
        if (!(nomenclature as MyOptionInterface)?.value) invalidFields.push(new RequiredFieldModel('Наименование ТРУ', RequiredFieldTypes.TABLE_ROW, idRow as string));

        // reportedYear
        if (!(reportedYear as MyOptionInterface)?.value) invalidFields.push(new RequiredFieldModel('Отчетный год', RequiredFieldTypes.TABLE_ROW, idRow as string));

        // reportedMonth
        if (!(reportedMonth as MyOptionInterface)?.value) invalidFields.push(new RequiredFieldModel('Отчетный месяц', RequiredFieldTypes.TABLE_ROW, idRow as string));

        // vatRate
        if (!(vatRate as MyOptionInterface)?.value) invalidFields.push(new RequiredFieldModel('Ставка НДС', RequiredFieldTypes.TABLE_ROW, idRow as string));

        // amount
        if (!amount) invalidFields.push(new RequiredFieldModel('Количество', RequiredFieldTypes.TABLE_ROW, idRow as string));

        // sum
        if (!sum) invalidFields.push(new RequiredFieldModel('Сумма', RequiredFieldTypes.TABLE_ROW, idRow as string))
        // else if (typeof (sum) === "string" && ((sum as string).includes(" ") || (sum as string).includes(","))) {
        else if (typeof (sum) === "string" && !(Boolean(Number(sum)))) {
            invalidFields.push(RequiredFieldsData.sumHasIllegalChars.sumHasIllegalChars);
        }

        // budgetItem
        if (!(budgetItem as MyOptionInterface)?.value) invalidFields.push(new RequiredFieldModel('Статья бюджета', RequiredFieldTypes.TABLE_ROW, idRow as string));

    });

    // console.log('validationCheckBlock3 invalidFields', invalidFields);
    return invalidFields;
}

export function validationCheckBlock4(myStore: MyStoreInterface, isToUpdate: boolean) {
    const dbData = myStore?.db4state;
    var invalidFields: Array<any> = [];

    //region
    // if (!dbData?.region?.value) invalidFields.push(RequiredFieldsData.db4.region);

    // budgetYear
    // if (!dbData?.budgetYear) invalidFields.push(RequiredFieldsData.db4.budgetYear);

    // description
    if (!dbData?.description) invalidFields.push(RequiredFieldsData.db4.description);

    if (isToUpdate) {
        if (!dbData?.comment) invalidFields.push(RequiredFieldsData.db4.comment);
    }

    // console.log('validationCheckBlock4 invalidFields', invalidFields);
    return invalidFields;
}