import Db1stateInterface from './Db1stateInterface';
import Db2stateInterface from './Db2stateInterface';
import Db3stateInterface from './Db3stateInterface';
import Db4stateInterface from './Db4stateInterface';

export type MyStoreInterface = {
    db1state: Db1stateInterface,
    db2state: Db2stateInterface,
    db3state: Db3stateInterface,
    db4state: Db4stateInterface,
    tableData: {},
    currencyData?: any,
    wasAlreadyPrefilled: boolean,
    isLoading: boolean,
    validationData?: {
      initialSum: number,
    }
}