import MyOptionInterface from "./MyOptionInterface";

class CustomField {
    id: string
    value: string | MyOptionInterface | MyOptionInterface[]

    constructor(id: string, value: string | MyOptionInterface | MyOptionInterface[]) {
        this.id = id;
        this.value = value;
    }
}

export default CustomField;