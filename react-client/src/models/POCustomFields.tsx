import CustomField from './CustomField';
import { Db2TabledataInterface } from './Db2stateInterface';
import { Db3MyTableStateInterface } from './Db3stateInterface';

class POCustomFields {
    provider?: CustomField
    contractNumber?: CustomField
    contractNumberLabel?: CustomField
    region?: CustomField
    budgetYear?: CustomField
    providerContactData?: CustomField
    providerId?: CustomField
    currency?: CustomField
    contractSum?: CustomField
    procurementType?: CustomField
    tableData?: CustomField | { id: string, value: Db2TabledataInterface[] }
    tableData2?: CustomField | { id: string, value: [{ row: Db3MyTableStateInterface }] }
    priority?: CustomField
    application4Spending?: CustomField
    contractsOptions?: CustomField
    appl4spendingAvailableOptionsCF?: CustomField
    regionAvailableOptions?: CustomField
    descriptionCF?: CustomField
    tableData2Labels?: CustomField | { id: string, value: [{ row: Db3MyTableStateInterface }] }
    guid?: CustomField

    constructor(provider: CustomField, contractNumber: CustomField, contractNumberLabel: CustomField,
        region: CustomField,
        budgetYear: CustomField, providerContactData: CustomField, providerId: CustomField,
        currency: CustomField, contractSum: CustomField, procurementType: CustomField,
        tableData: CustomField | { id: string, value: Db2TabledataInterface[] }, tableData2: CustomField, priority: CustomField,
        application4Spending: CustomField, contractsOptions: CustomField, appl4spendingAvailableOptionsCF: CustomField,
        regionAvailableOptions: CustomField, descriptionCF: CustomField, tableData2Labels: CustomField,
        guid: CustomField) {

        this.provider = provider;
        this.contractNumber = contractNumber;
        this.contractNumberLabel = contractNumberLabel;
        this.region = region;
        this.budgetYear = budgetYear;
        this.providerContactData = providerContactData;
        this.providerId = providerId;
        this.currency = currency;
        this.contractSum = contractSum;
        this.procurementType = procurementType;
        this.tableData = tableData;
        this.tableData2 = tableData2;
        this.priority = priority;
        this.application4Spending = application4Spending;
        this.contractsOptions = contractsOptions;
        this.appl4spendingAvailableOptionsCF = appl4spendingAvailableOptionsCF;
        this.regionAvailableOptions = regionAvailableOptions;
        this.descriptionCF = descriptionCF;
        this.tableData2Labels = tableData2Labels;
        this.guid = guid;
    }
}

export default POCustomFields;