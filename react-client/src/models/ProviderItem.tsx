export default interface ProviderItem {
    id: string;
    guid: string;
    name: string;
    isChecked?: boolean;
}