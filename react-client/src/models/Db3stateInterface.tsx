import MyOptionInterface from "./MyOptionInterface";

interface Db3stateInterface {
    datablock?: number,
    tableData: Db3MyTableStateInterface[],
    options: {
        clients: {
            available: MyOptionInterface[] | string,
            isLoading: boolean
        },
        organizationUnits: {
            available: MyOptionInterface[] | string,
            isLoading: boolean
        },
        nomenclatures: {
            available: MyOptionInterface[] | string,
            isLoading: boolean
            nomenAndUnitsLink?: [{
                nomenId: string,
                unitId: string,
            }],
            nomenAndBudgetItemLink?: [{
                nomenId: string,
                budgetItemLabel: string,
            }],
        },
        nomenclatureCharacteristics: {
            available: Map<String, Array<MyOptionInterface>>,
            isLoading: boolean
        },
        unitOfMeasurements: {
            available: MyOptionInterface[] | string,
            isLoading: boolean
        },
        budgetItems: {
            available: MyOptionInterface[] | string,
            isLoading: boolean
        }
    },
    currencyData?: {
        code: string,
        guid: string,
        name: string,
        rate: string,
    },
    isPrefilled?: boolean,
    budgetItemWasChanged?: boolean,
    totalSum?: number,
    totalSumInKzt?: number
}

export interface Db3MyTableStateInterface {
    idRow: number | string,
    client: MyOptionInterface | string,
    organizationUnit: MyOptionInterface | string,
    nomenclature: MyOptionInterface | string,
    nomenclatureCharacteristic?: MyOptionInterface | string,
    unitOfMeasurement: MyOptionInterface | string | undefined,
    productType: MyOptionInterface | string,
    reportedYear: MyOptionInterface | string,
    reportedMonth: MyOptionInterface | string,
    sum: number | string,
    sumInKzt: number | string,
    amount: number | string,
    vatRate: MyOptionInterface | string,
    budgetItem: MyOptionInterface | string | undefined,
    application4SpendingGuid: string,
}

export default Db3stateInterface