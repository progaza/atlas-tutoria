import { RequiredFieldModel } from '../core/constants/ProjectConstants';

export default interface AppStateInterface {
    activeBlock: string;
    issueAction?: string;
    childrensState: any;
    _showErrorDialog: boolean;
    _invalidFields: Array<RequiredFieldModel>;
    files?: File[];
    attachmentUploadProgress: number;
    filesUploaded: boolean;
    isLoading: boolean;
    isBannedBrowserUsed: boolean;
    showErrorScreen?: boolean;
    isSubmitted: boolean;
    issue: {
        key: string;
        id: string;
        guid: string;
    },
    sumWasIncreased?: boolean,
    budgetItemWasChanged?: boolean,
    systemTextStorage0: string,
}