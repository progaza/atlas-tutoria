import MyOptionInterface from "./MyOptionInterface";

interface Db4stateInterface {
    datablock?: number;
    region: {
        available: MyOptionInterface[],
        isLoading: boolean,
        value: MyOptionInterface | string,
    };
    budgetYear: MyOptionInterface | string;
    providerContacts: string;
    description?: string;
    isPrefilled?: boolean,
    comment?: string,
    issueKey?: string,
}

export default Db4stateInterface