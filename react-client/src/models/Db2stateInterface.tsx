import MyOptionInterface from "./MyOptionInterface";

interface Db2stateInterface {
    datablock: number,
    tableData: Db2MyTableStateInterface[],
    isPrefilled?: boolean,
}

export interface Db2MyTableStateInterface {
    idRow: number,
    documentType: MyOptionInterface | string,
    documentNum: string,
    documentDate: string
}

export interface Db2TabledataInterface {
    document: {
        date: string,
        number: string,
        type: string,
    }
}

export default Db2stateInterface