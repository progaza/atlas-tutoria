import MyOptionInterface from './MyOptionInterface';
import { OptionType, ValueType } from '@atlaskit/select';

export interface Db1stateInterface {
    datablock: number,
    issueAction?: string,
    username?: string,
    fullName?: string,
    organization?: string,
    department?: string,
    chosenContract?: {
        label: string,
        currency: string,
        sum: string,
        procurementType: string,
        value: string,
    },
    contractsOptions?: Array<MyOptionInterface>,
    priority?: MyOptionInterface | ValueType<OptionType>,
    provider?: {
        availableOptions: Array<MyOptionInterface>,
        linkedContracts: Array<{
            currency: string,
            label: string,
            procurementType: string,
            sum: string,
            value: string,
        }>,
        name: string,
        isLoading: boolean,
        isValid: boolean,
        chosen?: {
            chosenProvider: MyOptionInterface
        }
    },
    application4Spending?: {
        availableOptions?: Array<MyOptionInterface>,
        name: string,
        value: string,
        isLoading?: boolean,
        providerForApplications?: string,
        tableData?: Array<MyOptionInterface>,
    },
    isLoading?: boolean,
    isPrefilled?: boolean,
    issueKey?: string
};

export default Db1stateInterface;